% Ejemplo sencillo de detecci�n de eventos

clc, clear
f = @(t,y) (t.*y.^2);
y0=1; % c.inicial COL
tspan=[0 1.4];
opciones=odeset('Events',@evento);
[t,y,te,ye,ie]=ode45(f,tspan,y0,opciones);
format long g
te 
ye 
ie

tex=sqrt(2-2/50)
error=abs(te-tex)/tex
format

% te =
% 
%           1.39998813394001
% 
% 
% ye =
% 
%           50.0000000000444
% 
% 
% ie =
% 
%      1
% 
% 
% tex =
% 
%                        1.4
% 
% 
% error =
% 
%       8.47575713915281e-06
