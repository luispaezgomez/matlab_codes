clc,clear

f=@(x)(x.*sin(1./x));

% Como f no est� definida en 0, evito el valor 0 como primer punto de una 
% partici�n x=0:h:1 y empiezo directamente desde el segundo punto que es h:
x=10^-4:10^-4:1;
y=f(x);
figure(1)
plot(x,y,[0,1],[0,0],'k')

% Matlab no va a dibujar bien la parte que est� cerca de 0, por muy peque�o
% que hayamos puesto el paso. Si das zoom ves que al principio s� dibuja
% bien los ciclos pero a medida que te acercas a 0 lo distorsiona.

% Como tres �ltimos ceros entiendo yo que son los tres m�s cercanos a 1:

TOL=10^-12;
maxiter=100;

x0=0.3;
[sol,it,error]=minewton(f,x0,TOL,maxiter)

% sol =
% 
%     0.3183
% 
% 
% it =
% 
%      4
% 
% 
% error =
% 
%      0

x0=0.15; % x0=0.2; Le di primero este valor y se me va a otra ra�z
[sol,it,error]=minewton(f,x0,TOL,maxiter)

% sol =
% 
%     0.1592
% 
% 
% it =
% 
%      4
% 
% 
% error =
% 
%    1.7486e-15

x0=0.12;
[sol,it,error]=minewton(f,x0,TOL,maxiter)

% sol =
% 
%     0.1061
% 
% 
% it =
% 
%      6
% 
% 
% error =
% 
%    1.3878e-16

% Las soluciones son x=1/(n*pi), que para n=1,2,3 dan las tres ra�ces que
% se piden (con esta forma de nombrarlos no ser�an los tres �ltimos sino
% los tres primeros, los tres �ltimos no existir�an pues son infinitos):

x1=1/pi

x2=x1/2

x3=x1/3

% x1 =
% 
%     0.3183
% 
% 
% x2 =
% 
%     0.1592
% 
% 
% x3 =
% 
%     0.1061  % Que son los que hemos obtenido.

% Comprobaci�n gr�fica:

figure(2)
hold on
plot(x,y,[0,1],[0,0],'k')
plot(x1,0,'.r','MarkerSize',15)
plot(x2,0,'.r','MarkerSize',15)
plot(x3,0,'.r','MarkerSize',15)
hold off


