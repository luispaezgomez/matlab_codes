clc,clear
A=matriz(5);
midet=det(A)
mirank=rank(A)
micond=cond(A)
format short g
[L,U,P]=lu(A)
u55=U(5,5)
format
% 
% midet =
% 
%   -4.6043e-11
% 
% 
% mirank =
% 
%      4
% 
% 
% micond =
% 
%    1.6456e+17
% 
% 
% L =
% 
%             1            0            0            0            0
%        0.2963            1            0            0            0
%      0.037037      0.61173            1            0            0
%         0.125      0.90392        0.505            1            0
%        0.5787      0.74901       -0.255           -1            1
% 
% 
% U =
% 
%           216          343          512          729         1000
%             0        23.37       64.296          127        215.7
%             0            0       5.7052       20.311       47.011
%             0            0            0        -0.18        -0.72
%             0            0            0            0   8.8818e-15
% 
% 
% P =
% 
%      0     0     0     0     1
%      0     0     1     0     0
%      1     0     0     0     0
%      0     1     0     0     0
%      0     0     0     1     0
% 
% 
% u55 =
% 
%    8.8818e-15  

% Observaciones: A tiene determinante muy peque�o, aunque es no singular
% porque tiene rango 4. O sea, A es casi singular. El n�mero de condici�n
% es muy alto y cabe esperar mal comportamiento num�rico al resolver Ax=b.

% Cuando se multiplica A*x el resultado es una combinaci�n lineal de las
% columnas de A con coeficientes igual a las componentes del vector. En
% este caso, b=[0,1,0,1,0] y por lo tanto A*x=C2+C4 es la suma de las
% columnas pares.

b=A*[0,1,0,1,0]';
x=A\b

% Podemos comprobar que Matlab no puede obtener la soluci�n y nos avisa del
% mal condicionamiento:

% Warning: Matrix is close to singular or badly scaled. Results may be
% inaccurate. RCOND =  2.090747e-18. 
% > In Pb1 at 67 
% 
% x =
% 
%    -0.4482
%     2.7928
%    -2.6892  % Deber�a ser x=[0,1,0,1,0]'
%     2.7928
%    -0.4482  




