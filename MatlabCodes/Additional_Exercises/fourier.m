function y=fourier(x,N)
    
 y=0;
for k=1:N
    y=y+(1/(4*k^2-1))*cos(2*k*x);
end
 y=1/pi+0.5*sin(x)-(2/pi)*y;
return