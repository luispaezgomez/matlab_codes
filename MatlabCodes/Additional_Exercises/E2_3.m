clc,clear
format short g

f = @(t,y) (t.*y.^2);  % RHS de la EDO: podria ir a un fichero
t0=0;
y0=1;
tf=1.4;
yex=-2/(tf.^2-2);

%M�todo de Euler.
listaHeun=[];
h=0.1;
for i=1:6
    [T,y]=rkheun(f,t0,tf,y0,h);
    err=abs(y(end)-yex);
    listaHeun=[listaHeun; h err y(end) yex];
    h=h/2;
end
listaHeun



figure(1)
loglog(listaHeun(:,1),listaHeun(:,2),'ob-')

set(gca,'FontSize',18)
xlabel('h');ylabel('error')
legend('Heun','Location','Best')

%ajuste lineal para obtener la pendiente

ce=polyfit(log(listaHeun(:,1)),log(listaHeun(:,2)),1);
disp('Orden del M�todo de Heun:')
disp(ce(1))

% listaHeun =
% 
%           0.1       28.672       21.328           50
%          0.05       18.052       31.948           50
%         0.025       8.2667       41.733           50
%        0.0125       2.5428       47.457           50
%       0.00625      0.53844       49.462           50
%      0.003125     0.088299       49.912           50
% 
% Orden del M�todo de Heun:
%        1.6748

[T,Y]=ode45(f,[0,tf],y0);
Y(end)

% ans =
% 
%        50.042 %bastante m�s cerca de yex que el m�todo de Euler

%Se observa que Euler comete un error muy grande









