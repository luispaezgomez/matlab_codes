t=0:0.01:pi;
y=sin(t.^2);
plot(t,y)
f=@(x)(sin(x.^2));
x1=1.5;
tol=1e-12;
xcero=newton_tol(f,x1,tol)
% xcero =
% 
%     1.7725

Area=integral(f,0,xcero)

% Area =
% 
%     0.8948