syms x
f = x^2;
diff (f)
set(gca,'Color','red')
fplot (f)
set(gca,'FontSize',15)
title ('f´(x) & f(x)')
hold on
fplot (diff(f))