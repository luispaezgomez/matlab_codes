
clc,clear

% Cambiamos a sistema: Y'=f(t,Y) , donde Y=[y1(t);y2(t)] con el cambio 
% x(t)=y1(t); x'(t)=y2(t). y1(0)=0, y2(0)=-3;

f=@(t,y)([y(2);-2*y(1)-2*y(2)+4*cos(t)+2*sin(t)]);

opcion=odeset('RelTol',1e-12);
y0=[0;-3];
tspan=[0,5*pi];
[T,Y]=ode45(f,tspan,y0,opcion);
yp5pi=Y(end,2)

% yp5pi =
% 
%     -2.0000 Concuerda con la xex que nos da 
% xex'(t)=5exp(-t)sen(t)-5exp(-t)cost+2*cos(t). El seno en 5pi es 0 y el 
% coseno es -1. Por otro lado, 5pi es aprox 16 y exp(-16) es del orden de 
% 10^-7. Por lo tanto,
% xex'(5pi) es aprox. -2.

plot(T,Y(:,1),T,Y(:,2),[0,5*pi],[0,0],'k')

opciones=odeset('Events',@evento);
[T,Y,te,ye,ie]=ode45(f,tspan,y0,opciones);
format long g
te
ye
ie
format

% te =
% 
%     0.0000
%     0.9163
%     3.1417
%     6.2834
%     9.4250
%    12.5665
% 
% 
% ye =
% 
%    -0.0000   -3.0000
%     0.0000    1.5866
%    -0.0000   -1.7841
%     0.0000    1.9911
%    -0.0000   -1.9999
%     0.0000    2.0000
% 
% 
% ie =
% 
%      1
%      1
%      1
%      1
%      1
%      1

% Le hemos pedido que obtenga todos los ceros. En la lista de ceros aparece
% el cero inicial x(0)=0, el siguiente 0.91 y el segundo que se pide 3.1417

% La soluci�n exacta es x(t)=sen(t)(-5exp(-t)+2). Sabemos que el seno se 
% anula en todos los m�ltiplos de pi y el otro factor s�lo se anula una 
% vez: en t=-log(2/5) aprox 0.91. Observamos estos valores en la lista te. 
% A partir del segundo cero lo que yo observo en la gr�fica es la
% periodicidad de x(t), tener en cuenta que el t�rmino -5*exp(-t)*sin(t) va
% a decaer r�pidamente a cero y s�lo va a prevalecer el t�rmino peri�dico
% 2sen(t). Eso se puede ver en la gr�fica obtenida anteriormente. Tambi�n
% observamos que la funci�n de evento va calculando los ceros con casi
% 5 cifras exactas (nos dice 3.1417 en lugar de 3.1416).

% Si [T,Y] es la partici�n de tiempos junto con la soluci�n aproximada 
% obtenida por ode45 entonces la primera columna de Y son una tabla de 
% valores aproximados de xex(t) en los tiempos de la partici�n T. El error 
% absoluto m�ximo cometido ser� por lo tanto
% max_k abs(xex(T(k))-Y(k,1))

xex=@(x)sin(x).*(-5*exp(-x)+2);
error_max=max(abs(xex(T)-Y(:,1)))

% error_max =
% 
%    8.8486e-04

% No se pide, pero se puede ver d�nde se alcanza este error m�ximo:
error=abs(xex(T)-Y(:,1));
figure(2)
plot(T,error)






