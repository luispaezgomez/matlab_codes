% t=-10:0.01:10; % prob� primero con [-10,10] y vi que las intersecciones 
               % est�n en [0,7]

t=0:0.01:7;
y1=sin(t-3)+3;
y2=0.29*t+1.55;
plot(t,y1,t,y2)

f=@(x)(sin(x-3)+3-0.29*x-1.55);

tol=1e-10;
x1=1.6; %tomado de la gr�fica con Data Cursor
[xcero(1),it(1)]=newton_tol(f,x1,tol);

x1=1.84; %tomado de la gr�fica con Data Cursor
[xcero(2),it(2)]=newton_tol(f,x1,tol);

x1=5.88;   %tomado de la gr�fica con Data Cursor
[xcero(3),it(3)]=newton_tol(f,x1,tol);

format short g
xcero
it
format

% xcero =
% 
%        1.6047       1.8436       5.8827

% it =
% 
%      4     4     3


% Con fzero tambi�n salen los mismos valores:

x1=1.6; %tomado de la gr�fica con Data Cursor
xcero(1)=fzero(f,x1);

x1=1.84; %tomado de la gr�fica con Data Cursor
xcero(2)=fzero(f,x1);

x1=5.88;   %tomado de la gr�fica con Data Cursor
xcero(3)=fzero(f,x1);

format short g
xcero
format

Q=integral(f,xcero(2),xcero(3))

% Q =
% 
%     2.7009

% Un m�ximo tiene que anular la derivada. Aplico Newton a la derivada:
fp=@(x)(cos(x-3));
tol=1e-10;
x1=4.5; %tomado de la gr�fica con Data Cursor
[xcerop,it]=newton_tol(fp,x1,tol)

% xcerop =
% 
%     4.5708 % Es punto donde se alcanza el m�ximo absoluto
% 
% 
% it =
% 
%      3

% Como es una funci�n seno se sabe cu�l es el m�ximo absoluto:
% sin(x-3)=sin(t), x est� en [0,8] si y s�lo si t=x-3 est� en [-3,5]. El
% seno de t en [-3,5] lo dibujas a mano y ves que su m�ximo est� en t=pi/2. 
% Por lo tanto, x=3+pi/2 aprox 4.5708 que es lo que Newton ha obtenido.





