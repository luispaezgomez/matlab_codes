% Ejemplo sencillo de detecci�n de eventos

clc, clear
f = @(t,y) (t.*y.^2);
y0=1; % c.inicial COL
tspan=[0 1.4142];
opciones=odeset('Events',@evento);
[t,y,te,ye,ie]=ode45(f,tspan,y0,opciones);
format long g
te 
ye 
ie

tex=sqrt(2-2/100)
error=abs(te-tex)/tex
format

% te =
% 
%           1.40710787354346
% 
% 
% ye =
% 
%           100.000000000196
% 
% 
% ie =
% 
%      1

% tex =
% 
%           1.40712472794703
% 
% 
% error =
% 
%       1.19779030502938e-05
% 
% 










