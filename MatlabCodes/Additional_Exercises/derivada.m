%-------------------------------------------------------------------------
%                    DERIVADA NUM�RICA
%-------------------------------------------------------------------------

% Este fichero de funci�n obtiene la derivada num�rica de una funci�n
% escalar f(x), de un campo escalar de varias variables f(x,y,...) o de un
% campo vectorial F(x,y,...)=[F1(x,y,...);F2(x,y,...),...]. La matriz 
% derivada de un campo vectorial se tambi�n matriz jacobiana. No es necesario
% indicar el tipo de funci�n porque la propia programaci�n lo va a
% detectar.

function DF=derivada(F,x)
h=1e-5;
n=length(x); % n es el n�mero de inc�gnitas
ID=eye(n);
for k=1:n
xmash_k=x+h*ID(:,k);
xmenosh_k=x-h*ID(:,k);
DF(:,k)=(F(xmash_k)-F(xmenosh_k))/(2*h);
end
return

%Para hacer una prueba de que esto realmente funciona escribir en un script
%de prueba las instrucciones:

% F=@(x)[sin(x(1));cos(x(2));exp(x(3))];
% derivada(F,[0 pi/2 0]')
%Como F(x,y,z)=[sen(x);cos(y),e^z], el resultado debe ser la matriz 
%[1 0 0;0 -1 0;0 0 e].

% Nota: En el caso de un campo escalar f(x,y), la derivada es la matriz
% FILA con las derivadas parciales Df=[f_x,f_y]. El vector gradiente tiene
% las mismas componentes que Df, pero no es una matriz fila, es una
% COLUMNA: Grad(f)=[f_x;f_y]. Conviene tener esto en cuenta y no 
% confundirlos porque cuando aparecen en f�rmulas matem�ticas asociados a 
% operaciones matriciales, hay que usar las dimensiones adecuadas. Si donde 
% hay que poner Df pones Grad(f) dar� error al ejecutar porque las 
% dimensiones de las matrices no son las adecuadas. Para obtener 
% num�ricamente el gradiente bastar� ejecutar Grad=(derivada(f,x))'





