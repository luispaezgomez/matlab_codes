n=[1,10,100];
for k=1:3
    
    sol(k)=fourier(1,n(k));
end
sol
% sol =
% 
%     0.8274    0.8407    0.8415

sin(1)

t=0:0.01:2*pi;
for k=1:length(t)
y(k)=fun(t(k));
S2(k)=fourier(t(k),2);
S10(k)=fourier(t(k),10);
end
plot(t,y,t,S2,'r',t,S10,'k')

% Se observa que S10(t) ya es dif�cil distinguirla gr�ficamente de f(t)


 lista=[];

for  N=[1,10,100,1000]
    
    S=fourier(1,N);
    
    lista=[lista;abs(S-fun(1))];
   
end
format short g
lista
format

% lista =
% 
%      0.014117
%    0.00073861
%    6.3911e-07
%    1.8321e-08


N=1;
tol=1e-8;
error=tol+1;
while error >=tol
    
    S=fourier(1,N);
    
    error=abs(S-fun(1));
    N=N+1;
end
miN=N-1
error




% miN =
% 
%    510
% 
% 
% error =
% 
%    6.8471e-09

