clc,clear

f=@(t,y)([y(2);-2*exp(y(1))]);
tspan1=[0,2];
y0=[0;0];
opciones=odeset('RelTol',10^(-12));
[T1,Yode1]=ode45(f,tspan1,y0,opciones);

plot(T1,Yode1(:,1))

tspan2=[0,0.3,0.9,1.4];
[T2,Y2]=ode45(f,tspan2,y0,opciones);


sol(1)=Y2(2,1);
sol(2)=Y2(3,2);
sol(3)=-2*exp(Y2(4,1));
sol


%Euler Mejorado con h=0.1

Y0=[0,0]'; %vector inicial, lo ponemos como una COLUMNA porque 
%matem�ticamente su notaci�n es una columna.

t0=tspan1(1);
tf=tspan1(2);
h=0.1;  
t=[t0:h:tf]';  % vector de tiempo COLUMNA
n=length(t)-1; % numero de pasos
Y=Y0'; % condicion inicial, la hemos pasado a FILA porque la soluci�n del 
%PVI que vamos a obtener num�ricamente est� formada por tres columnas, cada
%una de las cuales es x(t), y(t), z(t), respectivamente:
%por lo tanto, Y(t)=[ x(t0) y(t0) z(t0);
%                     x(t1) y(t1) z(t1);
%                     ..................
%                     x(tN) y(tN) z(tN) ]

for k=1:n
    k1=f(t(k),Y0);
    k2=f(t(k)+0.5*h,Y0+0.5*h*k1);
    Y1=Y0+h*k2; % metodo de Euler mejorado (punto medio)
    Y=[Y;Y1'];
    Y0=Y1;
end

format short g
error=abs(Y(end,1)-Yode1(end,1)) % A falta de soluci�n exacta comparamos 
format                            %con la que nos proporciona ode45
% error =
% 
%      0.005726
% 


plot(t,Y(:,1),'r',T1,Yode1(:,1))

%Euler Mejorado con h=0.01

Y0=[0,0]'; %vector inicial, lo ponemos como una COLUMNA porque 
%matem�ticamente su notaci�n es una columna.
h=0.01;
t=[t0:h:tf]';  % vector de tiempo COLUMNA
n=length(t)-1; % numero de pasos
Y=Y0'; % condicion inicial, la hemos pasado a FILA porque la soluci�n del 
%PVI que vamos a obtener num�ricamente est� formada por tres columnas, cada
%una de las cuales es x(t), y(t), z(t), respectivamente:
%por lo tanto, Y(t)=[ x(t0) y(t0) z(t0);
%                     x(t1) y(t1) z(t1);
%                     ..................
%                     x(tN) y(tN) z(tN) ]

for k=1:n
    k1=f(t(k),Y0);
    k2=f(t(k)+0.5*h,Y0+0.5*h*k1);
    Y1=Y0+h*k2; % metodo de Euler mejorado (punto medio)
    Y=[Y;Y1'];
    Y0=Y1;
end

format short g
error=abs(Y(end,1)-Yode1(end,1))
format

% error =
% 
%    5.4072e-05 El resultado parece razonable porque el m�todo de Euler
%    mejorado es te�ricamente de orden 2. Con h=0.1 el error fue
%    aproximadamente 5*10^-3, al dividir h por 10 nos sale aprox 5*10^-5,
%    La diferencia entre -3 y -5 es justamente ese orden 2 del que
%    hablamos.

plot(t,Y(:,1),'r',T1,Yode1(:,1))

opciones=odeset('Events',@evento);
[t,y,te,ye,ie]=ode45(f,tspan1,y0,opciones);
format long g
te 
ye 
ie

% te =
% 
%           1.08503830654195  % Observe que se corresponde con la gr�fica
% 
% 
% ye =
% 
%          -1.00000000000002         -1.59011975310652
% 
% 
% ie =
% 
%      1
