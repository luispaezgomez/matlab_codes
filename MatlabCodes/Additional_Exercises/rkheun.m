
function [T,Y]=rkheun(f,t0,tf,y0,h)

%El m�todo de runge-kutta-heun tiene tablero
  
%     0  |  0
%    1/3 | 1/3
%    2/3 |  0  2/3
%    -----------------
%        | 1/4  0   3/4

t=[t0:h:tf]'; %vector de tiempos COLUMNA
%vector de tiempos COLUMNA
n=length(t)-1; %n�mero de pasos
Y=y0'; %valor inicial de la soluci�n num�rica. Al final del bucle, la 
       %soluci�n num�rica obtenida ser� {Y1,Y2,...,Y(n+1)}. En estos
       %m�todos cada t�rmino se calcula en funci�n del anterior. El primer
       %valor es [Y1(=y0)]. Debe ser una COLUMNA.
for k=1:n
    k1=f(t(k),y0);
    k2=f(t(k)+h/3,y0+h*k1/3);
    k3=f(t(k)+2*h/3,y0+2*h*k2/3);
    y1=y0+(h/4)*(k1+3*k3); %M�todo Runge-Kutta-Heun
    Y=[Y;y1']; %Cada vez que el bucle pasa por aqu�, se a�ade el �ltimo valor 
              %calculado al vector Y. Se usa ; en [Y;y1'] porque de esta
              %manera se construye una matriz en cuyas filas aparecen los
              %valores de Y_n.
    y0=y1; %Cuando tenemos una sucesi�n de recurrencia y(n+1)=g(yn), el yn 
           %es el que hace el papel del inicial y0, mientras que y(n+1) es
           %el siguiente, que hace el papel de y1. Al terminar un paso del
           %bucle, en el siguiente paso que calcular� y(n+2)=g(y(n+1)), el
           %papel de yn lo hace ahora y(n+1), por eso machacamos y0 con y1,
           %para que el siguiente paso por el bucle comience con la
           %iteraci�n que acabamos de obtener.
         
end
T=t;%vector de tiempos COLUMNA

