nodos=0:pi/4:pi;
valint=sin(nodos.^2);
format short g
c=polyfit(nodos,valint,4)

% c =
% 
%       0.40226      -2.1727       2.9505     -0.43548  -1.4701e-14

%El valor del �ltimo coeficiente sale casi cero porque en realidad es cero
%ya que el polinomio tiene que pasar por (0,0).

t=0:0.01:pi;
y=sin(t.^2);
polint=polyval(c,t);
plot(nodos,valint,'or',t,y,t,polint,'k')

mipolint=@(x)(polyval(c,x));

Q=integral(mipolint,0,xcero)

% Q =
% 
%       0.83898


format