
clc,clear
format short g

f = @(t,y) (t.*y.^2);  % RHS de la EDO: podria ir a un fichero

y0=1;
tf=1.4;
yex=-2/(tf.^2-2);

%M�todo de Euler.
listaEuler=[];
h=0.1;
for i=1:6
    t=[0:h:tf]';  % vector de tiempo COLUMNA
    n=length(t)-1; % numero de pasos
    y=zeros(n+1,1); % inicializamos a cero el vector y COL.
    y(1)=y0; % condicion inicial
    for k=1:n
        y(k+1)=y(k)+h*f(t(k),y(k)); % metodo de Euler
    end
   
    err=abs(y(end)-yex);
    listaEuler=[listaEuler; h err y(end) yex];
    h=h/2;
end
listaEuler



figure(1)
loglog(listaEuler(:,1),listaEuler(:,2),'ob-')

set(gca,'FontSize',18)
xlabel('h');ylabel('error')
legend('Euler','Location','Best')

%ajuste lineal para obtener la pendiente

ce=polyfit(log(listaEuler(:,1)),log(listaEuler(:,2)),1);
disp('Orden del M�todo de Euler:')
disp(ce(1))

% listaEuler =
% 
%           0.1       45.526        4.474           50
%          0.05       43.225       6.7752           50
%         0.025       39.739       10.261           50
%        0.0125        34.74        15.26           50
%       0.00625       28.218       21.782           50
%      0.003125       20.827       29.173           50
% 
% Orden del M�todo de Euler:
%       0.21946

% 
% Orden del M�todo de Euler:
%       0.21946

[T,Y]=ode45(f,[0,tf],y0);
Y(end)

% ans =
% 
%        50.042 %bastante m�s cerca de yex que el m�todo de Euler

%Se observa que Euler comete un error muy grande









