function y=fun(x)

if x>=0 && x<=pi
    y=sin(x);
elseif x>=pi && x<=2*pi
    y=0;
end