
function [tiempo1,tiempo2]=FuncionPCV2020(n)
% TODOS TIENEN TAMAÑO N
b = ones(n,1); % dp-1
c = b*-1; % dp+1
dp = zeros(n,1);
    for k=1:n
        dp(k,1)=2*k;
    end
A=spdiags([b,dp,c],-1:1,n,n);  
full(A)
% RESOLVER EL SISTEMA Ax=B
tic
x = A\b; % solucion del sistema
tiempo1=toc
tic
M = rand(n).*10
x1 = M\b;
tiempo2=toc
% Para n= 250, 500 y 1000
end