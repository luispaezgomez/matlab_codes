clc,clear

fun=@(t)exp(t.^2);

t=0:0.01:1;
y=fun(t);
figure(1)
plot(t,y)
nodos=0:0.25:1;
valint=fun(nodos);
N=length(nodos)-1;
c=polyfit(nodos,valint,N);
ypol=polyval(c,t);
hold on
plot(t,y,t,ypol,'r')
hold off
error=max(abs(y-ypol))
a0=integral(fun,0,1);
t1=-0.5:0.001:1.5;
sum=a0;
for n=1:40
    funsen=@(t)fun(t).*sin(2*n*pi*t);
    funcos=@(t)fun(t).*cos(2*n*pi*t);
    a=2*integral(funsen,0,1);
    b=2*integral(funcos,0,1);
    sum=sum+a*sin(2*n*pi*t1)+b*cos(2*n*pi*t1);
end

    
yex=fun(t1);
ypol=polyval(c,t1);
figure(2)
plot(t1,yex,'b',t1,ypol,'r',t1,sum,'g')

%     
%     
%     