%-------------------------------------------------------------------------
%              M�TODO DE NEWTON, para ecuaciones o sistemas
%-------------------------------------------------------------------------

% El m�todo de Newton explicado en clase se aplica a la resoluci�n de una
% ecuaci�n escalar f(x)=0 o un sistema de n ecuaciones con n inc�gnitas
% F(x)=0. La siguiente programaci�n lleva a cabo el m�todo en cualquiera de
% los dos casos, sin que tengamos que indicar nada. La derivada la obtendr�
% num�ricamente (llamando al archivo derivada.m) y el m�todo se parar�
% cuando o bien se alcance la tolerancia o bien se llegue al n�mero m�ximo
% de iteraciones. Las variables de salida ser�n el �ltimo t�rmino de la
% sucesi�n al que se llegue (sol), el n�mero de iteraciones (it) que se 
% llevaron a cabo para tener una idea de la rapidez del m�todo o de si se 
% lleg� al n�mero m�ximo de iteraciones y el error estimado (error).

function [sol,it,error]=minewton(F,x0,TOL,maxiter)
error=TOL+1;
it=0;
while error>=TOL && it<=maxiter
    x1=x0-derivada(F,x0)\F(x0);
    error=norm(x1-x0);
    x0=x1;
    it=it+1;
end
sol=x1;
return

% En la programaci�n estamos suponiendo que la derivada se va suministrar
% de manera num�rica, llamando al archivo derivada.m

% Se puede hacer una programaci�n donde la derivada sea una variable de
% entrada:

% function [sol,it,error]=minewton(F,DF,x0,TOL,maxiter)
% error=TOL+1;
% it=0;
% while error>=TOL && it<=maxiter
%     x1=x0-DF(x0)\F(x0);
%     error=norm(x1-x0);
%     x0=x1;
%     it=it+1;
% end
% sol=x1;
% return
% 

% Para ejecutar minewton anterior para resolver un sistema de ecuaciones, 
% por ejemplo {x^2+y^2=1, x=y} tendr�amos que escribir en un fichero de
% prueba aparte las siguientes instrucciones:

% F=@(x)[x(1)^2+x(2)^2-1;x(1)-x(2)];
% DF=@(x)[2*x(1) 2*x(2);1 -1];
% x0=[1;0]; % para obtener el corte en el primer cuadrante (ver dibujo)
% TOL=eps % o el valor que decidamos, 10^-6, 10^-10, etc.
% maxiter=20 % no se suele poner m�s de 100, porque Newton es un m�todo
% r�pido y si con 100 iteraciones no ha conseguido la tolerancia, mal
% asunto.
