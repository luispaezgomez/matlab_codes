A=magic(4);

detA= det(A)
% detA =
% 
%   -1.4495e-12
%Ax=b en teor�a tiene soluci�n �nica para cada b en R4. En la pr�ctica no
%se comportar� bien porque el determinante est� muy cerca de cero.

xex=[1 0 1 0]';
A*xex
% ans =
% 
%     19
%     15
%     15
%     19

b=[19 15 15 19]';
[L,U,P]=lu(A);
format short g
x=U\(L\(P*b))

EA=abs(x-xex)

C4U=U(:,4)

rankA=rank(A)

format

% x =
% 
%        1.3611
%        1.0833
%     -0.083333
%      -0.36111  %no tiene nada que ver con la soluci�n exacta
% 
% 
% EA =
% 
%       0.36111
%        1.0833
%        1.0833
%       0.36111
% 
% 
% C4U =
% 
%            13
%         -2.25
%        5.6667
%    3.5527e-15

%Observamos que el �ltimo pivote es muy peque�o, es lo que va a producir
%los fuertes errores que observamos. En este caso el problema est� mal
%condicionado:

% cond(A)
% 
% ans =
% 
%    2.7160e+16

%rankA =

%      3

%Matem�ticamente el rango es 4 pero matlab nos dice que el rango es 3
%porque realmente A est� m�s cerca de una matriz de rango 3 que de una
%matriz de rango 4.