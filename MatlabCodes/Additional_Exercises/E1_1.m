A=magic(4);
A(2,2)=6;
det(A)
% ans =
% 
%         6120
%Ax=b tiene soluci�n �nica para cada b en R4.

xex=[1 0 1 0]';
A*xex
% ans =
% 
%     19
%     15
%     15
%     19

b=[19 15 15 19]';
[L,U,P]=lu(A);
format short g
x=U\(L\(P*b))

EA=abs(x-xex)

C4U=U(:,4)

format

% x =
% 
%             1
%             0
%             1
%   -1.0657e-16
% 
% 
% EA =
% 
%             0
%             0
%             0
%    1.0657e-16  %Observe que el error es m�s peque�o que eps. Es lo que
%    cab�a esperar pues Matlab comete errores de redondeo y se transmiten
%    por el algoritmo magnificados por el n�mero de condici�n de A, que en
%    este caso es muy peque�o: 13.1538
% 
% 
% C4U =
% 
%            13
%         -2.25
%        4.8333
%        8.3607