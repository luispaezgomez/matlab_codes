
function [T,Y]=odesistemas
f= @(t,y)[-y(1)+6*y(2);-9*y(1)-2*y(2)];  %cambia la funcion tontito es lo que esta igualado a la derivada de la primera variable 
y0=[-1;2]; %cond iniciales % y la otra la de la segunda y(1)=1ºvariable e y(2)=2ºvariable
t0=0; tf=2;
tspan=[t0,tf]; %%[t0,1.5,tf] %%intervalo valores que quiero, si pones poner mas de 2 solo te da esos puntos
tol = 1e-10;
opts = odeset('RelTol',tol,'AbsTol',tol);
[T,Y]=ode45(f,tspan,y0,opts);
plot(T,Y(:,1),'b',T,Y(:,2),'k') %%grafica

%%para edo de segundo orden
%%y1=y  y2=y'
%%añadimos otra ecuacion y1'=y2 en el sistema
%%donde pone y'' poner y2' y las condiciones se cambia y por y1 e y' por y2
%%si no te aclaras mira ode45 explicativo