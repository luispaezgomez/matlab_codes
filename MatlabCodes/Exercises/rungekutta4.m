function rungekutta4
f= @(t,y) y+t; %% cambia la funcion tonto, pon lo que esta igualado a y'
h=0.1;  %% salto de un T a otro
t0=0; tf=1;  %%estos dos son entre los que evaluas
y0=1;   %%condicion inicial    
T=[t0:h:tf]'; %%intervalo donde evaluas
n=length(T)-1;  %% hay uno menos para designar el numero de tramos
YEM=zeros(n+1,1);
YEM(1)=y0;
for k=1:n  %% en k=1, f(t0,y0)
k1=f(T(k),YEM(k)); %% son formulas no hay que aprenderlo
k2=f(T(k)+0.5*h,YEM(k)+0.5*h*k1);
k3=f(T(k)+0.5*h,YEM(k)+0.5*h*k2);
k4=f(T(k)+h,YEM(k)+h*k3);
YEM(k+1)=YEM(k)+h*(k1/6+2*k2/6+k4/6+2*k3/6); % Formula para el metodo RK4
pendiente=(k1/6+2*k2/6+k4/6+2*k3/6);
end
YEM
yex=2*exp(T)-T-1;     %%esto es solo una demostracion de exactitud, no se puede con otras f
plot(T,yex,'b',T,YEM,'or')
error=abs(yex(end)-YEM(end))  


%% EXPLICACION
% Son metodos iterativos de paso constante para la aproximacion de
% soluciones de ecuaciones diferenciales ordinarias, en especial el
% problema de valor inicial.

% y'(t)=f(t,y(t))
% h es el paso por iteracion
% k son terminos de aproximacion intermedios evaluados en f de manera
% local
% k_i= f(t_n+h*c_i , y_n+h * sumatorio de a_ij*k_j)