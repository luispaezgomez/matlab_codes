function interpolarpol %% usar para pocos puntos eh mejor el spline
x=0:1.8:9; %%rango de valores , tambien podemos añadir rango de y o vectores en ambos
y=sin(4*log(x+1)); %%cambiar funcion
nodos=x;
valint=y;
N=length(nodos)-1;
c=polyfit(nodos,valint,N);
xx=0:0.01:9; %% cambiar este rango tmb
yy=polyval(c,xx);
plot(nodos,valint,'ob',xx,yy)

mipolint=@(x)(polyval(c,x)); %%calcular integra, definir primero la funcion 

Q=integral(mipolint,0,9);

z=polyval(c,2)-sin(4*log(2+1))
o=polyval(c,6)-sin(4*log(6+1)) %%errores

%%para los nodos dividir el valor entre nodos-1 , si nodos es 6 dividimos 9
%%entre 5