function x=root
c=[1 0 2 0 -2]; %% vector de coeficientes en este caso x^4+2x^2-2
x=roots(c);
% x.^4+2*x.^2-2=0;