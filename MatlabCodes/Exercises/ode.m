function [T,Y]=ode
f= @(t,y) sin(y)+cos(t);  %cambia la funcion tontito es lo que esta igualado a y'
y0=1; %%cond inicial
t0=0; tf=5; %%valores de t
tspan=[t0,tf]; %%[0,1.5,3] %%intervalo valores que quiero, si pones poner mas de 2 solo te da esos puntos
tol = 1e-10; %%tolerancia
opts = odeset('RelTol',tol,'AbsTol',tol);
[T,Y]=ode45(f,tspan,y0,opts);
plot(T,Y)
end