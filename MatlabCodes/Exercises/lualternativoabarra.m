function x=lualternativoabarra(A,b) %%A matriz b vector
[L,U,P]=lu(A);
x=U\(L\(P*b));
rank(A); %%rango
det(A); %%determinante
cond(A); %% numero de condicion, mientras mas cerca de 1 mejo