%%modifica la funcion y la dev tonto
function [sol,it,error1,error2,condicion]=newtonsistemas(x0,tol,maxiter) 
%%meter x0 como x0=[x0;y0]
f=@(x)[2*x(1)-x(2)-exp(-x(1));-x(1)+2*x(2)-exp(-x(2))]; %%funciones, todo a un lado f y g
df=@(x)[2+exp(-x(1)),-1;-1,2+exp(-x(2))]; %%derivadas en x e y de f y despues de g
it=0;
% Datos que dependen de enunciado -> tol=10^-8; x0=[1,2]; ,maxiter=10;
error1=tol+1;
error2=tol+1;
x1=x0;
while (error1>=tol || error2>=tol) && it<=maxiter
    d=df(x1)\f(x1);
    x2=x1-d;
    error1=abs(x1(1)-x2(1));  
    error2=abs(x1(2)-x2(2));   %%aqui no tocar tonto
    x1=x2;                                           
    it=it+1;
end
sol=x2;
condicion= cond(df(sol));
return