function dibujacurvas
x=-1:0.1:1;
y=-1:0.1:1; %%cambiar cuadrados
[X,Y]=meshgrid(x,y);
Z1=2*X-Y-exp(-X);    %%cambiar funciones
Z2=-X+2*Y-exp(-Y);
surf(X,Y,Z1)
hold on
surf(X,Y,Z2)
hold off
