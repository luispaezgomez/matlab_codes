function eulerbasic
f= @(t,y) y+t; %%cambia la funcion tonto es lo que esta igualado a y'
h=0.1;  %%salto de un T a otro
t0=0;   %%estos dos son entre los que evaluas
tf=1;
y0=1;  %%condicion inicial 
T=[t0:h:tf]';
n=length(T)-1;
Y=zeros(n+1,1);
Y(1)=y0;
for k=1:n
    Y(k+1)=Y(k)+h*f(T(k),Y(k));
end
yex=2*exp(T)-T-1; % la solucion exacta
Y
Error=abs(yex(end)-Y(end)) %%esto es solo una demostracion de exactitud o se puede con otras f
plot(T,yex,'b',T,Y,'o-k')

% La formula es x1 = x0 + h
%               y1 = y0 + h*f(x0,y0)