function [T,Y]=odeeventos
f= @(t,y) sin(y)+cos(t);  %cambia la funcion tontito es lo que esta igualado a y'
y0=1; %%cond inicial
t0=0; tf=5;
tol = 1e-8;
tspan=[t0,tf]; %%[0,1.5,3] %%intervalo valores que quiero, si pones poner mas de 2 solo te da esos puntos
opts=odeset('Events',@evento,'RelTol',tol,'AbsTol',tol); %%cambiar en eventos la funcion
[T,Y,te,ye,ie]=ode45(f,tspan,y0,opts);
plot(T,Y)
format short g
te
ye
ie