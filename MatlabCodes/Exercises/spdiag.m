%%crea una matriz con 3 diagnonales, puedes cambiar los numeros si los
%%quieres todos iguales o crear vectores nuevos y meterlos donde las e
%%vectores con el valor de la diag, diag 0 centro +1 1 mas arriba y -1 1
%%mas abajo, filas y columas
function  A=spdiag(n)
% n=3;
e=ones(n,1);
o=ones(n,1);
for i=1:n
o(i)=i*o(i); %% calcula un vector o=[1,2,3...]
end
A=spdiags([-e,o,3*e],[-1,0,1],n,n);
full(A)
end