function [T,Y]=odesistemaseventos
f= @(t,y)[y(2);-(9.8/2)*sin(y(1))];  %cambia la funcion tontito es lo que esta igualado a la derivada de la primera variable 
y0=[1.3;0]; %cond iniciales % y la otra la de la segunda y(1)=1ºvariable e y(2)=2ºvariable
t0=0; tf=20;
tol=10e-8;
tspan=[t0,tf]; %%[t0,1.5,tf] %%intervalo valores que quiero, si pones poner mas de 2 solo te da esos puntos
opts = odeset('Events',@evento,'RelTol',tol,'AbsTol',tol); %%cambiar en eventos la funcion
[T,Y,te,ye,ie]=ode45(f,tspan,y0,opts);
format short g
te
ye
ie