function rungekutta2
f= @(t,y) y+t;  %% cambia la funcion tonto, pon lo que esta igualado a y'
h=0.1;         
t0=0;  %% estos dos son entre los que evaluas
tf=1;
y0=1;  %% condicion inicial 
T=[t0:h:tf]';  %% intervalo donde evaluas
n=length(T)-1; %% hay uno menos para designar el numero de tramos
YEM=zeros(n+1,1);
YEM(1)=y0; %% condicion inicial en primera posicion
for k=1:n
k1=f(T(k),YEM(k)); %% en k=1, f(t0,y0)
k2=f(T(k)+0.5*h,YEM(k)+0.5*h*k1);
YEM(k+1)=YEM(k)+h*k2;  %% no se que es
end
YEM
yex=2*exp(T)-T-1;    %% esto es solo una demostracion de exactitud, no se puede con otras f
plot(T,yex,'b',T,YEM,'or')
error=abs(yex(end)-YEM(end)) 
