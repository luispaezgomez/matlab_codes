function interpolarsplinefijabordes
x=0:0.4:2; %%rango de valores , tambien podemos añadir rango de y o vectores en ambos
y=x.^2+rand(1,length(x)); %%cambiar funcion
nodos=x;
valint=y;
xx=0:0.01:2; %% hay q cambiar este rango tmb
der1=(y(2)-y(1))/(x(2)-x(1));  %%puede darlos el enunciado o elegirlos 
der2=(y(end)-y(end-1))/(x(end)-x(end-1));
yy=spline(nodos,[der1,valint,der2],xx);
plot(nodos,valint,'ob',xx,yy)