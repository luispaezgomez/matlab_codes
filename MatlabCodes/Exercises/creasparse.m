%%matriz con 2 diagonales perpendiculares y guarda en un sparse
function A=creasparse(n,p,q)
v=ones(1,n);
v=p*v;
B=diag(v); % mete v en la dp de una matriz tamaño lenghth v*lenghth v
z=ones(1,n);
z=q*z;
C=diag(z);
C=rot90(C);
A=B+C;
A=sparse(A);
end


%%otra forma sparse(vector con las filas,vector columnas,vector elemento)