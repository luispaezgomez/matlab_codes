%%suma las columas pares de a que tiene tamaño n
function b=creavectores(a,n)
b=zeros(1,n);
for i=1:n
    for j=1:n
        if(mod(j,2)==0)  %%resto
        b(i)=b(i)+a(i,j);
        end
    end
end
end