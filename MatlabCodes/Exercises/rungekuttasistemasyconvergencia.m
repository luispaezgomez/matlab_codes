function rungekuttasistemasyconvergencia %%esto esta para solo dar el valor final
f=@(t,y)[y(2);-2*exp(y(1))];  %%esto es lo de sistemas
Y0=[0;0];    
t0=0;tf=20;

yex=-38.6137056388822;

H=4*0.1.^(0:3); %%mira si te dan la H
for m=1:length(H)
h=H(m);   %%esto es para varias h si es una sola quitar el primer for y poner valor h directamente
t=t0:h:tf;
n=length(t)-1;
Yrk=Y0;    %% si quiero todos los valores Yrk(1)
for k=1:n
    k1=f(t(k),Yrk);
    k2=f(t(k)+0.5*h,Yrk+0.5*h*k1);
    k3=f(t(k)+0.5*h,Yrk+h*0.5*k2);  
    k4=f(t(k)+h,Yrk+h*k3);
    Yrk=Yrk+h*(k1+2*k2+2*k3+k4)/6; %%Yrk(k+1)=Yrk(k), si quiero todos los valores
end
ErrorRK(m)=abs(yex-Yrk(1,1)); %%calcular error
end


nodos=log(H);  %%tiene que haber varias h para usarlo
valintRK=log(ErrorRK);
cRK=polyfit(nodos,valintRK,1);  %%todo esto es para calcular el orden
ordenRungeKutta=cRK(1)           %%vale pare euler y otros rungekutta
loglog(H,ErrorRK,'o-')
