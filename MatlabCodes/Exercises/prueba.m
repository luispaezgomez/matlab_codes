f = @(t,y)[y(2);-2*exp(y(1))];
y0=[0,0];
tspan = [0,2];
[T,Y]=ode45(f,tspan,y0);
subplot(3,1,1)
%figure(1)
plot(T,Y(:,1),'r',T,Y(:,2),'b') % SOLUCION -> y
subplot(3,1,2)
%figure(2)
plot(Y(:,1),Y(:,2),y0(1),y0(2),'or') % trayectoria o fase del EDO
subplot(3,1,3)
%figure(3)
plot(1:(length(T)-1),diff(T)) % como ODE ha elegido los pasos de la particion


