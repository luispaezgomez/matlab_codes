function [x,fval,flag,resumen]=solve
f=@(x)[3*x(1).^2-x(2).^2;3*x(1).*x(2).^2-x(1).^3-1]; %meter f y g todo a un lado (dejando 0 al otro lado)
% hay dos variables
x0=[4;-1]; %meter punto de inicio
optimset('fsolve');
opciones=optimset('TolX',1e-10,'TolFun',1e-10); %%cambiar tolerancia
[x,fval,flag,resumen]=fsolve(f,x0,opciones); %%meter punto inicio