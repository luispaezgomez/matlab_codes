%%modifica la funcion y la dev tonto
function [sol,it,error,valor,condicion]=newton(x0,tol,maxiter)
it=0;
error=tol+1;
x1=x0;
while error>=tol && it<=maxiter
    x2=x1-(-1+x1.^2)/(2*x1); %% arriba original
    error=abs(x1-x2);                                  %% abajo derivada
    x1=x2;                                             %% con x1
    it=it+1;
end
sol=x2;
valor=-1+x2.^2; %%aqui original con x2
condicion=2*x2;  %%aqui derivada con x2
return