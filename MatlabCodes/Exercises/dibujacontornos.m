function dibujacontornos
x=-1:0.01:1;
y=-1:0.01:1; %%cambiar cuadrados
[X,Y]=meshgrid(x,y);
Z1=3*X.^2-Y.^2;    %%cambiar funciones
Z2=-3*X.*Y.^2-X.^3-1;
contour(X,Y,Z1,[0,0],'b')
% para dibujar en y=0,x=0, --> [0,0]
hold on
contour(X,Y,Z2,[0,0],'r')
hold off




[x,fval,flag,resumen]=solve
f=@(x)[3*x(1).^2-x(2).^2;3*x(1).*x(2).^2-x(1).^3-1]; %meter f y g todo a un lado (dejando 0 al otro lado)
% hay dos variables
x0=[4;-1]; %meter punto de inicio
optimset('fsolve');
opciones=optimset('TolX',1e-10,'TolFun',1e-10); %%cambiar tolerancia
[x,fval,flag,resumen]=fsolve(f,x0,opciones); %%meter punto inicio