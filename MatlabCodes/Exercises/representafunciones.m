%%cambia la funcion tonto
x = linspace(2-sqrt(3),2+sqrt(3),1000); % aqui se ponen los limites del eje mayor
m =linspace(-1,4);
y=m.^2-3*m+2;
z=sqrt(2*(1-((x-2).^2)/3))+1; % raiz positiva elipse
l=-sqrt(2*(1-((x-2).^2)/3))+1; % raiz negativa elipse
plot(m,y,'b',x,z,'r',x,l,'r')
ax = gca;
ax.XAxisLocation = 'origin';
ax.YAxisLocation = 'origin';