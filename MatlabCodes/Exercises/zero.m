function [x,fval,flag,resumen]=zero
f=@(x) x.^2-3*x+2-(sqrt(2*(1-((x-2).^2)/3))+1); %meter f
opciones=optimset('Display','iter','TolX',1e-8); %%cambiar tol
[x,fval,flag,resumen]=fzero(f,0.5,opciones); %%meter punto inicio