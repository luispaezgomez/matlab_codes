% ¡IMPORTANTE VITAL DE ODE45!
% Cuando resolvemos ecuaciones de segundo orden, sabemos que hay que
% obtener un sistema, con ODE -> PRIMERA COLUMNA DE Y VALORES DE Y 
% SEGUNDA COLUMNA DE Y VALORES DE Y'
% dependiendo del orden de f=@(t,y)[...;...]

% ¡Importante!
% Cuando llamo a una funcion que esta en otro fichero es
% indispensable que la funcion y el fichero tengan el mismo
% nombre, en caso contrario no funciona

% ¡Importante!
% Cuando pidan el valor x_0 para el que f(x_0)=2 
% ES SIMPLEMENTE LA INTERSECCION DE Y=2 CON NUESTRA FUNCION

% DERIVADA DEFINICION
f = @(x) x.^2;
x=[0:0.001:4];
h=1;
fp=@(x)(f(x+h)-f(x))/h; % estimacion nuemrica de la derivada

%plot(x,f(x),'b',cero1,0,'sk',x,zeros(size(x)),'r') Representa una linea
%recta con size zeros y en elcero encontrado un square
% EJEMPLO DE MARCAR UN ZERO
f=@(x)x.^2-1;
x=[-3:0.01:5];
plot(x,f(x))
cero=fzero(f,1.5)
hold on
linea=zeros(size(x))
plot (cero,0,'sr',x,linea)

% EJEMPLO ODE45
f = @(t,y)sin(y)+cos(t);
y0=1;
tspan = [0,2,4,5];
% F sin parentesis y sin nada
[T,Y]=ode45(f,tspan,y0)
% Meto en una variable el valor de y(4) que es la tercera componente de  Y
y4 = Y(3,1);
valor = 2*sin(y4)+cos(4)

% REPRESENTAR ODE45 SIN SER SISTEMA
f = @(t,y)sin(y)+cos(t);
y0=1;
tspan = [0,5]; % Importante que no halla mas de dos numeros
[T,Y]=ode45(f,tspan,y0);
plot(T,Y)

% REPRESENTAR ODE45 CON SISTEMA
f = @(t,y)[y(2);-2*exp(y(1))];
y0=[0,0];
tspan = [0,2];
[T,Y]=ode45(f,tspan,y0);
subplot(3,1,1)
%figure(1)
plot(T,Y(:,1),'r',T,Y(:,2),'b') % SOLUCION -> y
subplot(3,1,2)
%figure(2)
plot(Y(:,1),Y(:,2),y0(1),y0(2),'or') % trayectoria o fase del EDO
subplot(3,1,3)
%figure(3)
plot(1:(length(T)-1),diff(T)) % como ODE ha elegido los pasos de la particion

% EJEMPLO DE PERMUTACION
a = [1,2;3,4];
d = a(1,:)+77; % Sumamos 77 a la primera fila y guardamos en d
a (1,:)= d; % Permutamos, actualizamos el valor de la primera fila
b = [1;2];
det (a);
inv (a);

% ¡¡¡PLOT REFACHERO!!!
 f=@(x)sin(4*(log(x+1)));
 x=[0:0.001:9];
 plot(x,f(x),'LineWidth',2)
 hold on
 title 'sin(4*(log(x+1)))'
 minx = 1.15*min(x); 
 maxx = 1.15*max(x);
 miny = 1.15*min(f(x)); 
 maxy = 1.15*max(f(x));
 plot([minx,maxx],[0,0],'k','LineWidth',2)
 plot([0,0],[miny,maxy],'k','LineWidth',2)
 plot(maxx,0,'>k','markersize',15,'MarkerFaceColor','k')
 plot(0,maxy,'^k','markersize',15,'MarkerFaceColor','k')
 text(maxx,0.3,'X','fontsize',15,'color','k')
 text(-0.7,maxy,'Y','fontsize',15,'color','k')

% EJES PARA FUNCIONES
x=[-100:0.01:100]
plot(x,sin(x-3)+3-0.29*x-1.55)
ax=gca;
ax.XAxisLocation='origin';
ax.YAxisLocation='origin';

% GUARDAR EL TIEMPO DE PLOT EN UNA VARIABLE Y EN UNA LISTA
tic;
xn=A\b; 
tn=toc; % cputime de \
tic; 
[L,U,P]=lu(A);
tlu1=toc;
tic;
xlu=U\(L\(P*b));
tlu2=toc;
lista=[lista;n tn tlu1 tlu2 tlu1+tlu2];

% MARCADORES GRÁFICAS (DESPUES DEL PLOT)
-xlabel
-ylabel
            ylabel ('Humedad')
-text
            x = 0:pi/20:2*pi;
            y = sin(x);
            plot(x,y)
            text(pi,0,'\leftarrow sin(\pi)')
-title
            title('My Title')

% PARA CAMBIAR FORMATO DE LOS EJES + LEYENDA
ezplot ('2*sin(x)*cos(x)',[-5,5])
set (gca, 'linewidth',7)
legend ('LoQueQuierasPoner','Location','best')

% REGLAS PARA VARIABLES
    -No espacio
    -No comenzar por numeros
    -No caracteres especiales
    -Barra baja permitada

% colorbar es para marcar una escala de colores
% Para fsolve, Newton, etc hay que poner la
% variable, no como (x,y), sino como variable vectorial x=[x(1);x(2);..] y
% también la función F(x)=[F1(x);F2(x);...]
% POLARPLOT -> polares // PLOT -> cartesianas
% Para en plot unir por segmentos poner 'ob-' 
% Funciones sin punto -> sen,cos,exp(e^x),log(ln(x)),sqrt(x^1/2)
% round -> redondear
% rand -> aleatorio entre [0,1]
% Elemento a elemento el punto va antes que el signo * / ^
% ' -> de fila a columna, pero tambien conjuga
% cosh, sinh, asin, acos
% format long/short/rat/long e, ans
% disp ('a*b=')  disp (a*b)
% a/a2 a*inv(a2) <---> a\a2 inv(a)*a2
% donde este la parte mas alta del slash significa inversa
% ./ \. requiere que las dos matrices tenga la misma dimension
% Matriz con strings -> A=['BIENVENIDO' ' A' ' MI EXAMEN']
c = a\b;
[m,n]=size(a); 
% para guardar el tamaño de una matriz en dos variables
% length en matrices devuelve la m o n que sea mas grande
% SIEMPRE 'TolX',1e-10,'TolFun',1e-10 EN ODE Y SOLVE

% MATRICES ESPECIALES
           -eye (n) matriz unidad
           -zeros (n,m)
           -ones (n,m)
           -hilb (n) matriz de hilbert
           -magic (n) matriz magica
           -pascal (n)

% MATRIZ POR BLOQUES PERO DIAGONAL
A1 = ones(2,2);
A2 = 2*ones(3,2);
A3 = 3*ones(2,3);
B = blkdiag(A1,A2,A3)

% EXTRAER LA PARTE TRIANGULAR SUPERIOR E INFERIOR
A=rand(4)*10;
triu(A)
tril(A)

% MATRIZ HILBERT
function H=matrizHilbert(n)
% generamos la matriz de Hilbert
H=zeros(n); % inicializamos la matriz
for i=1:n
    for j=1:n
        H(i,j)=1/(i+j-1);
    end
end
end
% n=4;
% A=creamatriz(n) % comprobar si la matriz es nula
% Z=A-A';
% para comprobar si es la matriz nula
% a:b:c de a hasta c saltando un tamaño b
% a:c de a hasta c saltando de uno en uno
% linspace (a,b,n) intervalo [a,b] en n puntos
% A=ceil(10*rand(n)); crea una matriz aleatoria
% ceil aproxima al numero mas grande EJEMPLO 3.4->4 -1.9->1
% graphic current axis (gca)
% show current figure (shg)
% existe ezplot,ezplot3,ezmesh,ezcontour,ezmeshc

% ATAJOS MATRICES
%          -a (2;) fila 2 de a
%          -a (;2) columna 2 de a --> a(;2)=0 columna 2 de a es 0
%          -a (1;2,4) extraer de la primera fila -> a(1,2) a(1,3) a(1,4)
%          -a (2,4;1) extraer de la primera columna -> a (2,1) a(3,1) a(4,1)
%          -help matlab/matfun todas las funciones de matrices
%          -spy(A) PLOT DE UNA MATRIZ

                             %% APUNTES %%
% CALCULAR CERO Y MARCARLO
f=@(x)x.^2-1;
x=[-3:0.01:5];
plot(x,f(x))
cero=fzero(f,1.5);
hold on
linea=zeros(size(x));
plot (cero,0,'sr',x,linea)

% ¡¡¡ORDEN DE RUNGE-KUTTA!!!
% Este metodo lo que hace es siempre modificar la misma componente del vector de soluciones
f=@(t,y)[y(2);-2*exp(y(1))];
Y0=[0;0];
t0=0;tf=20;
yex=-38.6137056388822;
H=4*0.1.^(0:3); 
for m=1:length(H)
h=H(m);
t=t0:h:tf;
n=length(t)-1;
Yrk=Y0;
for k=1:n
    k1=f(t(k),Yrk);
    k2=f(t(k)+0.5*h,Yrk+0.5*h*k1);
    k3=f(t(k)+0.5*h,Yrk+h*0.5*k2);
    k4=f(t(k)+h,Yrk+h*k3);
    Yrk=Yrk+h*(k1+2*k2+2*k3+k4)/6;
end
ErrorRK(m)=abs(yex-Yrk(1,1));
end
nodos=log(H);
valintRK=log(ErrorRK);            %LO IMPORTANTE PARA OBTENER EL GRADO DE RK
cRK=polyfit(nodos,valintRK,1);
ordenRungeKutta=cRK(1)

figure(2)
loglog(H,ErrorRK,'o-')

format short g
tablaerrores=[H',ErrorRK']

% CREAR MATRIZ CON SPDIAG Y CON DIAG
function A = matriz(n)
A=zeros(n); % inicializamos
e=ones(n-1,1);
A=diag([1:n])-diag(e,+1)+diag(e,-1);
end
function A = funmatrizsparse(n)
% Generamos la matriz sparse
% SIN PASAR POR LA VERSION FULL
e=ones(n,1)
d=[1:n]';
AS=spdiags( [e d -e], [-1 0 1],n,n);
end

% MATRIZ POR BLOQUES
format short g
n=4;
As=10*rand(n);
uno=ones(n,n)
M =[ uno 2*uno 3*uno;...  % Digamos que mete en una matriz otras matrices
    4*uno 5*uno 6*uno;... % Entonces en la matriz resultado se pueden ver el resto de matrices
    7*uno 8*uno 0*uno]
spy(M)

% DIBUJAR LINEA PARA MARCAR ZERO
x=0.5:0.01:5;
y=f(x);
plot(x,y,[0.5,5],[0,0],':k') % Dibuja ademas una linea para marcar elcero

% DIBUJAR RECTAS
plot ([0,2*pi],[0.5,0.5]) --> dibuja la recta desde el pto (0,0.5) hasta (2*pi,0.5)
% dibuja dos puntos y los une

% NUMEROS COMPLEJOS
i,j, abs(modulo), angle, conj

% CREAR NUEVO FICHERO RAPIDAMENTE
edit NuevoFichero

% MANERAS DE DEFINIR UNA FUNCION
function dydt=fun(t,y)   % miembro derecho de la ec. y'=f(t,y)
dydt=y+t.*cos(t);
end

x = [0:0.01:1]
f = @(x) x.^2+2

x = [0:0.01:1]
y = x^.2

% DIFERENTES MANERAS DE USAR PLOT
f =@(x) 4*log (sin(x));
x = [0:0.01:2]
plot (x,f(x))

y = x.^3+sqrt(x);
x = [0:0.001:1];
axis=([0 2 0 2])
plot (x,y)
title 'funcion'
set (gca,'FontSize',18)

x = [0:0.01:1];
plot (x,fun(x))
function nombre=fun(x)   
nombre=x+x.*cos(x);
end

% CREAR MATRIZ
a (m,n)
a (n,n)
x1 = [1,2];
x2 =[3,4];
a = [x1,x2];

% CREAR FUNCIONES (fichero y nombre de funcion igual)
function y=NombreFichero(x)
y = cos(x)
return / end
% Y va a ser la variable de salida, x la variable de entrada
% Para darle valor en x escribir NombreFichero([1,2,3,4])

% FUNCION QUE DEVUELVE SUMA
function z=suma(x,y) % recibe x,y -> devuelve z
z = x+y;
return

% AYUDA
help, helpwin, lookfor

% LISTAAAAAAAAA
lista=[]
...
lista=[lista;n toc ....]

% SUMAR COLUMNAS DE A
b=zeros(1,n)';
for k=1:n
    b=b+A(:,k);
end
disp(b)

xex = ones(n,1); % vector de unos
suma = H*xex; % suma de todas las columnas de H, donde H es matriz de Hilbert

% SUMAR FILAS DE A
b=zeros(1,n)';
for k=1:n
    b=b+A(k,:);
end
disp(b)

% EXTRAER MATRIZ DE MATRIZ
magica = magic(3)
extr = magica(1:2,1:2)

% ELIMINAR COLUMNA DE MATRIZ
a(:,2)=[]

% ELIMINAR FILA DE MATRIZ
a(2,:)=[] 

% AÑADIR FILA A MATRIZ
a = [a ; [1,2,3]]
% también como a =[1,2;3,4]; a(2,3)=1;

% AÑADIR COLUMNA A MATRIZ
a = [a [1;2;3]]

% COMANDO FOR
for j=inicio:paso:final
end
BREAK -> interrupcion inmediata
CONTINUE -> continua por donde iba
for j=vector
end

% while, switch, elseif todos con end al final

% inv, det, trace, eig

% trace (A) == sum(diag(A))

% AUTOVALORES MATRIZ
eig (a)

% AUTOVECTORES MATRIZ (en la salida un vector de dos componentes)
[v1,v2] = eig (a)

% PERMUTACION DE FILAS O COLUMNAS
% Sea una matriz 2*2
a = rand(2)
a = a([2,1],[1,2]) , % he intercambiado la fila 2 con la 1, para hacerlo con
                     % las columnas es necesario a = a([1,2],[2,1])

% RELLENAR MATRIZ COMPONENTE A COMPONENTE DEL 1 AL 10
C (1:10,1)=[1:10]'
C (1,1:10)=[1:10]    %columna a columna de la primera fila

% FUNCION QUE DEVUELVE TRAZA Y DET
function [P,v]=dettra(A,B)
P=A*B;
v=[det(P),trace(P)] ;
return
% en otro fichero -> [H,Q]=dettra([1;2;3],[4,5,6])

% FUNCIONES INLINE
% estructura -> nombre = @(x,y)(x^2)
f = @(x,y)(x.^2)
f( [1,2,3,4] ,2)

% CAMPOS VECTORIALES
% estructura -> nombre = @(x,y)(x^2;y+x)
f = @(x,y)(x.^2;y+x)

% SEGMENTOS
x=[0,1];y=[2,3];
plot(x,y)

% PLOT
% hay que hacer mallado primero 
x = [0:0.01:1]
y = x.^2
plot (x,y)
axis([0 2 0 2])
axis equal % todos los ejes iguales
axis tight % encaja el dibujo en el mallado
grid on % pone una cuadricula
title ('..')
% GRID ON EN 3D ES PARA DAR SENSACION DE VOLUMEN
hold on, hold off
b     blue          .     point              -     solid
g     green         o     circle             :     dotted
r     red           x     x-mark             -.    dashdot 
c     cyan          +     plus               --    dashed    
m     magenta       *     star             (none)  no line
y     yellow        s     square
k     black         d     diamond
w     white         v     triangle (down)
                    ^     triangle (up)
                    <     triangle (left)
                    >     triangle (right)
                    p     pentagram
                    h     hexagram

% SIEMPRE PONER EL . ANTES DE * / ^

% PLOT3
% hay que poner dos funciones en y, x y en z el intervalo 
t = 0:pi/50:10*pi;
x = sin(t);
y = cos(t);
z = t;
plot3(x,y,t)

% EZPLOT3
ezplot3('3*cos(t)','t*sin(t)','sqrt(t)')

%SURF -> SUPERFICIES (RECTANGULOS) MESH -> CURVITAS
x = [-7:0.5:7]; % dibujamos rectangulo en el plano xy
y = [-4:0.5:4];
% COGER 0.5 PORQUE SE QUEDA SIN COLORES
surf (x,y,z)
clc,clear
x = [-7:0.5:7]; % dibujamos rectangulo en el plano xy
y = [-4:0.5:4];
[x,y]= meshgrid (x,y);
z = tan(x.^2+y.^2)-1

% LIMITES DE MÁQUINA
eps % numero mas pequeño
realmax
realmin

% FUNCIONES MATRICES SPARSE
spdiags,sprank,speye,spones,rot 90 (gira 90 grados)
find -> encuentra indices de entradas no nulas
full (Formato normal) <-> sparse
spy(A) -> grafica de las posiciones no nulas
nnz(A) -> num de entrasdas no nulas de una matriz
% para las normales ->diag,rank,eye ones
% spdiags sirve para crear matrices rellenando diagonales

% SINTAXIS SPARSE A = sparse (fil,col,x)
% Ejemplo a = sparse ([1,1,2,2,3],[1,2,1,3,3],[1,2,1,1,2])
%         a = full (a)

% CONSTRUCCION DE MATRIZ 3X3 SPARSE
d1 = [4,5,0]';
d2 = [1:3]';
d3 = [0,6,7]';
a=spdiags([d1,d2,d3],-1:1,3,3)
full (a)
% HAY QUE METER 0 AL PCIPIO O FINAL
% d1,d2,d3 son los vcetores que queremos meter en diagonales
% -1,0,1 desplazamiento de diagonal respecto a la dp
% 3x3 dimension de matriz

% CONSTRUCCION DE MATRIZ 3X3 SPARSE SOLO DP
d1 = [4,5,7]';
d2 = [1:3]';
d3 = [0,6,7]';
a=spdiags([d1],[0],3,3)
full (a)

% condest(A), cond(A) -> numero de condicion para invertir
% nºcondicion = norma de A * norma de inv(A)
% A partir del 1000 no es fiable las operaciones con esa matriz
% nnz -> non zero elements, numel -> number of elements
% DIAG SIRVE PARA EXTRAER DIAGONALES O PARA CREAR MATRICES DIAGONALES
n=3;
Am=10*rand(n)
A = round(Am)
vdiag = diag (A,1)
B=diag(diag(A)) % creamos una matriz con solo una diagonal

% EXTRAER TODAS LAS DIAGONALES NO NULAS DE A Y COLOCAR EN B
[B,d]=spdiags(A)
% En b se guardan las diagonales no nulas
% En d se guarda la posicion de la diagonal

% EXTRAER DIAGONALES ESPECIFICADAS
B = spdiags (A,d)
% Si d=0 se extrae la dp
% extrae las diagonales especificadas por d y las guarda en b

% REEMPLAZAR DIAGONALES DE A ESPECIFICADAS POR D POR LAS COLUMNAS DE B
A = spdiags (B,d,A)

% CREAR MATRIZ SPARSE MXN CUYAS DIAGONALES SE EXTRAEN DE LAS COLUMNAS DE B
A = spdiags (B,d,m,n)
B = [b1,b2,...] 
d = [d1,d2,...] D1 va relacionado con b1
% n no puede ser mayor que el numero de filas de B
A sera sparse y no tiene pq ser cuadrada

% NOTA: EN -1 (POR DEBAJO DE DP) LO DEL FINAL ES IGNORADO MIENTRAS QUE EN 1
% LO DE ARRIBA ES IGNORADO

% DIBUJAMOS CURVAS DE NIVEL
x =-1:0.01:1;
[x,y]=meshgrid(x);
z = x.^3+y.^2;
n = numero de curvas de nivel
contour (x,y,z,n)
% para dibujar en y=0,x=0, --> [0,0]
% Eso en donde este n

% OTRAS FUNCIONES PARA DIBUJAR -> FIMPLICIT, FPLOT, FSURF

% EZPLOT NO ESTÁ RECOMENDADO MATLAB RECOMIENDA FPLOT
% EJEMPLO FPLOT
fplot(@(x) exp(x),[-3 0],'r-')
% EJEMPLO EZPLOT
ezplot ('2*sin(x)*cos(x)',[-5,5])


% EJEMPLO DE FIMPLICIT 
f = @(x,y)x.^2+y.^2-1;
fimplicit (f)

% FACTORIZACION LU
% PA=LU 
% Ax=b->PAx=Pb->(PA=LU) Lux=Pb 
[L,U,P]= lu (A)
x = U\(L\(P*b));
% [luis,rojo,cielo]= lu(A)
% los nombres no dan error siempre LUP

% MATRIZ HILBERT
% Ejemplo de matriz mal condicionada
a = 1/(i+j-1)
b = hilb (n)

% FIGURE (1), para separar en graficas

% INTERPOLACION
coef = polyfit (nodos,valint,n) -> obtiene los coeficientes de un polinomio de ajuste de grado n
nodos = [x1,x2,x3...]
valint = [y1,y2,y3...]
N NUMERO DE X QUE QUIERO QUE SALGAN
p(x)=p_1*x^n+p2*x^n-1...
y = polyval (c,x) -> evalua el polinomio de coeficientes c en un punto x o una particion x
% cuando hay muchos nodos y valint se produce el fenomeno de runge
% N <length (nodos) -1 ---> polinomio de ajuste
% N =length (nodos) -1 ---> polinomio de interpolacion
% los dos se calculan con polyfit

% EJEMPLO INTERPOLACION
n=5; % Numero de nodos
% SIHAY MUCHOS NODOS, FENOMENO DE RUNGE
% N+1 Valores pq son los coeficientes de un polinomio de grado N
% N+1 incognitas
% N=2 SIGNIFICA QUE EL POLINOMIO DE INTERPOLACION ES DE GRADO UNO
f = @(x)sin(x.^2);
nodos = linspace (0,pi,n);
valint = zeros (1,n);
 for k=1:n
     valint(1,k)=f(nodos(k));
 end
N = length(nodos)-1;
% Tenemos los nodos evaluados en la funcion
coef = polyfit (nodos,valint,length(nodos)-1) % de grado n
sparse(pol)
% En pol estan los coeficientes 
mallado = linspace(min(nodos),max(nodos));
% Cuando no pones nada al final va a sacar 100
yy = polyval(coef,xx);
% En polyval se evaluan los coeficientes hallados en pol
plot(nodos,valint,'.','MarkerSize',20)
hold on
plot(mallado,yy)
x = [0:0.001:pi];
plot(x,f(x))
% nodos = [x1,x2,x3...]
% valint = [y1,y2,y3...]
% y = polyval (c,x) -> evalua el polinomio de coeficientes c en un punto x o una particion x
% cuando hay muchos nodos y valint se produce el fenomeno de runge
% N <length (nodos) -1 ---> polinomio de ajuste
% N =length (nodos) -1 ---> polinomio de interpolacion
% los dos se calculan con polyfit

% INTERPOLACION CON PUNTOS
X= [-2 -1 0 2 5];
Y= [0 3 5 11/2 6];
n= length(X)-1; % grado del Pn
% polinomio de interpolacion
pn=polyfit(X,Y,1)
xi=[-2:0.01:5];
% dibujamos tambien el spline
sp=spline(X,Y,xi);
plot(X,Y,'bo',...
    xi,polyval(pn,xi),'r',...
    xi,sp,'k')
axis([-3 6 -1 7])

%% SPLINE
% Spline Not A Knot 
x = particion o un solo valor
y = spline (nodos,valint,x)
devuelve los valores de spline NAK en los puntos x
% Spline Sujeto
x = particion o un solo valor
y = spline (nodos,[der1,valint,der2],x)
dato 1 y dato 2 son las derivadas en los puntos donde empieza el intervalo
devuelve los valores de spline sujeto en los puntos x
para elegir la derivada, calculamos la pendiente del segmento
der1 = (y(2)-y(1))/(x(2)-x(1));
der2 = (y(end)-y(end-1))/(x(end)-x(end-1));
% Ejemplo
x = 0:10;
y = sin(x);
xq = 0:0.25:10;
yq = spline(x,y,xq);
figure
plot(x,y,'o',xq,yq)

% ESCALAS LOGARITMICAS
clc,clear
x =[10^-1,10^-3,10^-4,10^-8];
y = rand (1,4).*x;
figure(1)
plot (x,y,'ob')
axis ([-0.1,0.2,-0.1,0.2])
figure (2)
loglog (x,y,'ob')
 
% FPRINTF 
cadena = '%4.2f Esto es una prueba %4.3f';
fpritnf (cadenadeformato, lista);
% % saca 4 cifras, 2 son decimales en float
% saca 4 cifras, 3 son decimales en float
%d decimal, %f float, %e exponente, \n salto de linea
A1 = [1,2,3]
A2 = [3,4,5]
fprintf (cadena,A1,A2)
%EN GENERAL MEJOR USAR DISP

% EJEMPLO DE POLARPLOT
theta = 0:0.01:2*pi;
rho = sin(2*theta).*cos(2*theta);
polarplot(theta,rho)

% ODE45
[T,Y]=ode45 (fun,tspan,y0)
    fun -> funcion de dos variables (t,y) que recoge el RHS 
fun = @(t,y)k*y+t;
    tspan -> intervalo donde varia t, o una seleccion de valores t
tspan =[0,2]
    y0 -> valor de la condicion inicial

% ODE45 SEGUNDO ORDEN
% ode45 solo resuelve ecuaciones diferenciales de primer orden
    ¡¡¡¡¡¡¡¡¡¡¡¡HACER EL SIGUIENTE CAMBIO!!!!!!!!!!!!!!!!
                   y1=y    Para las condiciones iniciales  --> Es decir si                               CONDICIONES y1=0 --> y1'=y2   SISTEMA
                   y2=y'   hacer lo mismo                        y(0)=0 --> y1=0, y'(0)=-3 --> y2 =-3    INICIALES   y2=-3 -->  y2'=......
% En el sistemas de ecuaciones siempre tiene y1'=y2;
% Convertir una ecuacion diferencial de segundo orden en un sistema de ecuaciones diferenciales

%% EVENTOS %%
% Es el valor de x que verifica una ecuacion -> la ecuacion se verifica
% Para utilizar eventos neesitamos definir una funcion en otro archivo a
% parte
% La función de evento debe tener 3 variables de salida
        -Funcion cuyo cero queremos calcular
        -ISTERMINAL -> '0' Si queremos todos los valores de t donde hay evento
         ISTERMINAL -> '1' Primer valor de t donde ocurre el evento
        -DIRECTION  -> '1' Buscara ceros donde la funcion del evento es creciente
         DIRECTION  -> '-1' Buscara ceros donde la funcion del evento es decreciente
         DIRECTION  -> '0' Buscara ceros con los dos casos

% FUNCION EVENTO EN OTRO FICHERO
function [mifun,isterminal,direction]= evento(t,y)
    mifun = y(1);
    isterminal = 0 ;
    direction = -1;
return
end
% ODESET INTRODUCE OPCIONES PARA ODE45
opciones = odeset ('Events',@evento)
% ESTRUCTURA ODE45+EVENTOS
[T,Y,te,ye,ie]=ode45(f,tspan,y0,opciones)
        -te -> valores del tiempo done ocurrio el evento
        -ye -> valores de Y en el evento
        -ie = 1 -> comprobacion de que es un valor que buscamos (evento en el te encontrado)
        -ie = 0 -> el evento ocurre pero para un te  no encontrado

% AJUSTES PLOT SIEMPRE VAN DESPUES DEL PLOT
plot (............)
set(gca,'FontSize',15)

% DERIVADAS, INTEGRALES
integral, diff

% SIMBOLICO
syms x,clc
g = @(x) x.^2.*log(x);
f = x.^2;
diff (f); % PRETTY sirve para poner la fracción bonita
pretty (ans)
F= int (f,x);
pretty (F)
q = integral (g,0,2)

% EJEMPLO DE SPLINE
function InterpolarSpline
x=[0:1.8:9];
y=sin(4*log(x+1)); % funcion a hacer spline
nodos=x;
valint=y;
xx=[0:0.01:9];
yy=spline(nodos,valint,xx);
plot(nodos,valint,'ob',xx,yy)
end

% EJEMPLO INTERPOLAR
function interpolarpol % Usar para pocos puntos
x=[0:1.8:9];
y=sin(4*log(x+1)); % cambiar funcion
nodos = x;
valint = y;
N = length(nodos-1);
coef = polyfit (nodos,valint,N);
xx = [0:0.01:9]; % cambiar el rango
yy = polyval (coef,xx)
plot (nodos,valint,'ob',xx,yy)

mipolint = @(x)(polyval(coef,x)); % calcular, integra, definir primero la funcion

Q = integral ( mipolint,0,9);
z = polyval(coef,2)-sin(4*log(2+1))
o = polyval (coef,6)-sin(4*log(6+1)) 
% errores
end

% EJEMPLOS DIBUJAR ELIPSE
x = linspace(2-sqrt(3),2+sqrt(3),1000); % aqui se ponen los limites del eje mayor
m =linspace(-1,4);
y=m.^2-3*m+2;
z=sqrt(2*(1-((x-2).^2)/3))+1; % raiz positiva elipse
l=-sqrt(2*(1-((x-2).^2)/3))+1; % raiz negativa elipse
plot(m,y,'b',x,z,'r',x,l,'r')
ax = gca;
ax.XAxisLocation = 'origin';
ax.YAxisLocation = 'origin';


% DIBUJAR ELIPSE
x = [2-3^1/2:0.01:2+3^1/2];
j = @(x)1+(2-2/3*(x-2).^2).^1/2 %  Ecuacion despejada de la elipse
x = [2-3^1/2:0.01:2+3^1/2];
g = @(x)1-(2-2/3*(x-2).^2).^1/2
h = @(x)x.^2-3*x+2;

t = -pi:0.01:pi;
a = 3^1/2;
b = 2^1/2;
x1 = 2;
y1 = 1;
x=x1+a*cos(t);
y=y1+b*sin(t);
plot (x,y)
hold on
x = [-4:0.001:4];
plot (x,x.^2-3*x+2)


f = @(x)1+(2-2/3*(x-2).^2).^1/2-(x.^2-3*x+2);
df= @(x)(4/3*(x-2)-2/3*(2*x-4))/2*sqrt(2-2/3*(x-2).^2)-(2*x-3)  % derivada analítica

x0=0;   %probar con distintas x0= -5, -1.5, 0, 20
niter=10;
x1b=newtonescalar(f,df,x0,niter)
%SOLO CALCULA LA INTERSECCION CON LA POSITIVA

function x=newtonescalar(fun,dfun,x0,numiter)
for k = 1:numiter
    d = fun(x0)/dfun(x0);
    x = x0-d;
    fprintf('x(%2d)=%16.15f \n',k,x)   
    pause(0.1)  
    x0 = x;
end
end

% EJEMPLO FOURIER
%        | 1 0<=x<pi
% x(t)= <|
%        | -1 pi<=x<2*pi
clear all
clc
t=0:0.001:2*pi;
xt=(t>=0)+(t>=pi).*-2;
plot(t,xt,'b','LineWidth',1.5)
ax=gca;
ax.XAxisLocation='origin';
ax.YAxisLocation='origin';
hold on
grid on
% SF
k=100; % iteraciones
s=0; % suma
for n=1:k
    p=(2/(n*pi))*(1-(-1)^n).*sin(n*t);
    s=s+p;
    if (n==k)
    plot(t,s,'g')
    pause(1)
    end
end

% OTRO EJEMPLO DE FOURIER PERO NO FUNCIONA
%        | sin(t) 0<=x<pi
% x(t)= <|
%        | 0 pi<=x<2*pi
clear all
clc
t=0:0.0001:2*pi;
xt=(t<=pi).*sin(t)+(t>=pi)*0; % final opcional
plot(t,xt,'b','LineWidth',1.5)
ax=gca;
ax.XAxisLocation='origin';
ax.YAxisLocation='origin';
hold on
grid on
% SF
k=5; % iteraciones
s=0; % suma
for n=1:k
    p=1/pi+1/2.*sin(t)-2/pi*(cos(2*n*t)/(4*n.^2-1));
    s=s+p; 
    if(n==k)
    plot(t,s,'g')
    pause(0.25)  
    end
end

% EJEMPLO FOURIER FUNCION X
%        | 
% x(t)= <|  x   -pi<x<pi
%        | 
clear all
clc
t=-pi:0.0001:pi;
xt=t; % final opcional
plot(t,xt,'b','LineWidth',1.5)
ax=gca;
ax.XAxisLocation='origin';
ax.YAxisLocation='origin';
hold on
grid on
% SF
k=500; % iteraciones
s=0; % suma
for n=1:k
    p=2/n*(-1)^(n+1)*sin(n*t);
    s=s+p; 
    if(n==k)
    plot(t,s,'g')
    pause(0.25)  
    end
end