n=5;
b = zeros (n,1); 
k=1; % contador
while (k<=n)
    resto = mod (k,2);
    if (resto == 1)
    b (k,1)=1;
    end
    k=k+1;
end
unos = ones (n,1);
vectordp = zeros (n,1);
k=1;
while (k<=n)
    vectordp (k)=k;
    k=k+1;
end
% Los vectores que se meten por encima/debajo de la dp TIENEN TAMAÑO N

A=spdiags([unos vectordp unos],-1:1,n,n);

tic
% Ax=b , x = A\B
sol = A\b
toc
format long
sol(1)
sol(n)

c = magic (n)
tic
% Cx=b, x = C\B
solmagic = c\b
toc
sol(1)
sol(n)
