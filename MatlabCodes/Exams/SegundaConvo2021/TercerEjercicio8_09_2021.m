% e.^(x), exp(x)

f = @(x)1+exp(x).*cos(x).^2
x = [0:0.01:2];
plot (x,f(x))
set (gca, 'fontsize',18)
integral (f,0,2)
