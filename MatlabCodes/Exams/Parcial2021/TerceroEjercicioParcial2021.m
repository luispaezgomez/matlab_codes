f = @(t,y)[y(2);-2*exp(y(1))];
y0=[0,0];
tspan = [0,0.3,0.9,1.4,2];
[T,Y]=ode45(f,tspan,y0)
%subplot(3,1,1)
%figure(1)
%plot(T,Y(:,1),'r',T,Y(:,2),'b') % SOLUCION -> y
%subplot(3,1,2)
%figure(2)
%plot(Y(:,1),Y(:,2),y0(1),y0(2),'or') % trayectoria o fase del EDO
%subplot(3,1,3)
%figure(3)
%plot(1:(length(T)-1),diff(T)) % como ODE ha elegido los pasos de la particion

% La matriz Y tiene en la primera columna y(1), lo que buscamos es y(2)
y = Y(2,2) % meto la coordenada que equivale al tiempo 0.3 en nuestro vector Y

% Para calcular y'(0.9)
yaux = Y(3,2)
valor = -2*exp(yaux)

% Para calcular y''(1.4)
yauxaux = Y(4,2)
valor2 =  