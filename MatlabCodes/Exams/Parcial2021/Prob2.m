clc, clear
%------------------------------------------------------------------------
%            Apartado 1
%------------------------------------------------------------------------

F=@(x,y)3*x.^2-y.^2;
G=@(x,y)3*x.*y.^2-x.^3-1;
x=-5:0.01:5;
y=x;
[X,Y]=meshgrid(x,y);
Z1=F(X,Y);
Z2=G(X,Y);
hold on
contour(X,Y,Z1,[0,0],'b')
contour(X,Y,Z2,[0,0],'r')
plot([-5,5],[0,0],':k')
plot([0,0],[-5,5],':k')
hold off

% Observamos dos cortes, uno en el primer cuadrante y otro en el cuarto
% cuadrante. He importado los valores aproximados de los cortes mediante la
% herramienta Data Cursor(debes poner clc,clear en comentario para que al
% ejecutar no te borre el P1 y P2 creados)
% P1
% P2
% P1 = struct with fields:
%     Target: [1�1 Contour]
%     Position: [0.4900 0.9200 0.1266]
% P2 = struct with fields:
%     Target: [1�1 Contour]
%     Position: [0.4900 -0.8600 -0.0304]
% Se observa que el punto del primer cuadrante es aproximadamente (0.5,0.9)
% y el del cuarto cuadrante (0.5,-0.9)

%------------------------------------------------------------------------
%            Apartado 2
%------------------------------------------------------------------------

% Como se ha explicado en clase, para fsolve, Newton, etc hay que poner la
% variable, no como (x,y), sino como variable vectorial x=[x(1);x(2);..] y
% tambi�n la funci�n F(x)=[F1(x);F2(x);...]

F1=@(x)3*x(1).^2-x(2).^2;
F2=@(x)3*x(1).*x(2).^2-x(1).^3-1;
F=@(x)[F1(x);F2(x)];
x0=[0.5;0.75];
[x,fval,exitflag,output] = fsolve(F,x0)

% En Comand Window puedes observar:

% Equation solved.
% 
% fsolve completed because the vector of function values is near zero
% as measured by the default value of the function tolerance, and
% the problem appears regular as measured by the gradient.
% 
% <stopping criteria details>
% 
% 
% x =
% 
%     0.5000
%     0.8660 % soluci�n del sistema
% 
% 
% fval =
% 
%    1.0e-08 *
% 
%    -0.2119   
%     0.3179
% % Valor de F(x), te�ricamente es 0, en la pr�ctica un algoritmo que 
% converge lo que calcula es x tal que F(x) es muy peque�o
% 
% exitflag =
% 
%      1 % Indica que el algoritmo converge
% 
% 
% output = 
% 
%   struct with fields:
% 
%        iterations: 3
%         funcCount: 12
%         algorithm: 'trust-region-dogleg'
%     firstorderopt: 1.1928e-08

%------------------------------------------------------------------------
%            Apartado 3
%------------------------------------------------------------------------
   
% Despejando como dice el enunciado:
f=@(x)sqrt((1+x.^3)./(3*x))-2;
% Para saber d�nde se anula y comprobar si hemos aplicado bisecci�n bien
% podemos pedir una gr�fica:
x=0.5:0.01:5;
y=f(x);
plot(x,y,[0.5,5],[0,0],':k') % Dibuja ademas una linea para marcar el cero
% Observa la gr�fica, aproximadamente en 3.5 se anula, en 3 est� por debajo
% y en 4 est� por arriba. Tomo el intervalo [3,4] para bisecci�n (la
% funci�n mediante while y for se ha dado en clase). En el enunciado se
% desprende que usemos while con TOL=10^-4

TOL=10^-4;
[x,k,error]=bisec2(f,3,4,TOL)

function [x,k,error]=bisec2(f,a,b,TOL)
k = 0;
  while b-a > TOL
     x = (a + b)/2;
     if sign(f(x))==sign(f(a))
        a=x;
        else
        b=x;
     end
k=k+1;
%observe que esta instrucci�n no aparec�a en bisec1, pues va implicita en 
% el for. En este caso, cada vez que el bucle pase por ella sumar� un valor 
% y la variable k nos estar� sirviendo como contador del n�mero de 
% iteraciones que el programa realiza una vez completa el bucle.
  end
error=b-a;
end

% x = 3.4217 % ra�z obtenida, se corresponde con la gr�fica
% 
% k = 14 % numero de iteraciones de bisecci�n
% 
% error = 6.1035e-05 % El error exacto no puede conocerse, pero esto es una
% cota superior del error ya que sabemos que la ra�z est� entre los �ltimos
% a y b calculados por el algoritmo de bisecci�n. Por lo tanto, podemos
% estar seguros de que hemos calculado la ra�z con 4 cifras exactas, como
% pide el enunciado. El enunciado pide error relativo y hemos puesto
% absoluto en el bucle, no pasa nada, no es demasiado diferente para un
% n�mero de este tama�o.

