clc,clear

% Probamos que nuestra funci�n est� bien construida
n=5;
A=mimatriz(n);
full(A)
% ans =
% 
%      1     3     0     0     0
%     -1     2     3     0     0
%      0    -1     3     3     0
%      0     0    -1     4     3
%      0     0     0    -1     5

lista=[];
N=10.^(2:6);
for k=1:length(N)
    n=N(k);
    A=mimatriz(n);
    solex=ones(n,1);
    b=A*solex;
    tic;
    xnum=A\b;
    toc;
    error=norm(solex-xnum)/norm(solex);
    lista=[lista;n toc error];
end
format short g
lista
format

% tras varias ejecuciones el resultado obtenido es:
% lista =
% 
%           100    0.0006272   1.0878e-16
%          1000     6.33e-05   3.8937e-17
%         10000    0.0006994   1.2658e-17
%         1e+05    0.0025106   4.1392e-18
%         1e+06     0.024062   1.3822e-18

% En el caso de no usar la orden sparse el resultado es:

n=10^3;
v1=-ones(n-1,1);
v2=(1:n)';
v3=3*ones(n-1,1);
A=diag(v1,-1)+diag(v2)+diag(v3,1);
% Nota: tambi�n vale A=full(mimatriz(n)); pues full convierte una matriz
% sparse en una ordinaria.
solex=ones(n,1);
b=A*solex;
tic;
xnum=A\b;
toc;    
error=norm(solex-xnum)/norm(solex);
format short g
listanosparse=[n toc error]
format
% listanosparse =
% 
%          1000    0.0054805   3.2747e-17
% Ya sabemos que el tiempo de ejecuci�n var�a cada vez, pero m�s o menos
% est� en torno a 5*10^-3 frente a 6e-05 de la matriz sparse (es decir,
% 100 veces mayor cuando la matriz es no sparse).

