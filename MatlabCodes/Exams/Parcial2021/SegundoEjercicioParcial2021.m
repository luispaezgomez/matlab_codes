function SegundoEjercicioParcial2021
x=-1:0.01:1;
y=-1:0.01:1; %%cambiar cuadrados
[X,Y]=meshgrid(x,y);
Z1=3*X.^2-Y.^2;    %%cambiar funciones
Z2=-3*X.*Y.^2-X.^3-1;
contour(X,Y,Z1,[0,0],'b')
% para dibujar en y=0,x=0, --> [0,0]
hold on
contour(X,Y,Z2,[0,0],'r')
hold off
[x,fval,flag,resumen]=solve_21 %%meter punto inicio