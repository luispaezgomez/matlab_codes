function [x,error,k]=bisec_21(a,b,tol) %%poner un a por debajo del valor que quieres
k=0;                                %%poner un b por encima
f=@(x)(((1+x.^3)/3.*x).^0.5)-2;
while b-a>tol
x=(a+b)/2;
if sign(f(x))==sign(f(a))
 a=x;
else
 b=x;
end
k=k+1;
error=b-a;
end
return
