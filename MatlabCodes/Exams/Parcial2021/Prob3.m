clc,clear
format long g

%------------------------------------------------------------------------
%            Apartado 1
%------------------------------------------------------------------------

% y1'=y2;
% y2'=-2*exp(y1)
%y1(0)=0; y2(0)=0;

F=@(t,y)[y(2);-2*exp(y(1))];
Y0=[0;0];
t0=0;tf=2;
tspan=[t0,tf];

[T,Y]=ode45(F,tspan,Y0);

figure(1)
plot(T,Y(:,1))

tspan=[t0,0.3,0.9,1.4,tf];

[T,Y]=ode45(F,tspan,Y0)

% T =
% 
%                          0
%                        0.3
%                        0.9
%                        1.4
%                          2
% 
% 
% Y =
% 
%                          0                         0
%        -0.0886817694230607        -0.582625521292649
%         -0.719661235877724         -1.43259545282192
%          -1.53177154527526         -1.77070295337594
%          -2.65000545701013         -1.92805474969792

% Los datos que se piden son:

% PRIMERA COLUMNA VALORES DE Y
% SEGUNDA COLUMNA VALORES DE Y'
y03=Y(2,1)
yp09=Y(3,2)
ypp14=-2*exp(Y(4,1))

% y03 =  -0.0886817694230607
% 
% 
% yp09 = -1.43259545282192
% 
% ypp14 = -0.432304808323797

%------------------------------------------------------------------------
%            Apartado 2
%------------------------------------------------------------------------

f=@(t,y)[y(2);-2*exp(y(1))];
Y0=[0;0];
t0=0;tf=20;

yex=-38.6137056388822;

H=4*0.1.^(0:3); 


for m=1:length(H)
h=H(m);
t=t0:h:tf;
n=length(t)-1;

Yrk=Y0;
for k=1:n
    k1=f(t(k),Yrk);
    k2=f(t(k)+0.5*h,Yrk+0.5*h*k1);
    k3=f(t(k)+0.5*h,Yrk+h*0.5*k2);
    k4=f(t(k)+h,Yrk+h*k3);
    Yrk=Yrk+h*(k1+2*k2+2*k3+k4)/6;
end
ErrorRK(m)=abs(yex-Yrk(1,1));


end

nodos=log(H);
valintRK=log(ErrorRK);
cRK=polyfit(nodos,valintRK,1);
ordenRungeKutta=cRK(1)

figure(2)
loglog(H,ErrorRK,'o-')

format short g
tablaerrores=[H',ErrorRK']

% ordenRungeKutta =
% 
%        4.0664
% 
% 
% tablaerrores =
% 
%             4        36.07 elevado a 0
%           0.4    0.0049588 elevado a 1
%          0.04   2.6645e-07 elevado a 2
%         0.004    2.666e-11 elevado a 3

% Se observa en la tabla anterior que con h=4*10^-2 se aseguran los 7
% d�gitos (m�s o menos).

% Lo comprobamos:

h=4*10^-2;
t=t0:h:tf;
n=length(t)-1;
Yrk=Y0;
for k=1:n
    k1=f(t(k),Yrk);
    k2=f(t(k)+0.5*h,Yrk+0.5*h*k1);
    k3=f(t(k)+0.5*h,Yrk+h*0.5*k2);
    k4=f(t(k)+h,Yrk+h*k3);
    Yrk=Yrk+h*(k1+2*k2+2*k3+k4)/6;
end
format long g
Yrk=Yrk(1,1)
yex
% Yrk = -38.613705372433
% yex = -38.6137056388822
% Puedes observar la coincidencia en los 7 primeros d�gitos.

%------------------------------------------------------------------------
%            Apartado 3
%------------------------------------------------------------------------

F=@(t,y)[y(2);-2*exp(y(1))];
Y0=[0;0];
t0=0;tf=4;
tspan=[t0,tf];
[T,Y]=ode45(F,tspan,Y0);
% Exploramos gr�ficamente para ver c�mo es la gr�fica y en qu� momento
% alcanza el valor -5 (usando tf=2 puedes ver que a�n no ha cortado a
% la recta y=-5). Puedes apreciar que ocurre en torno a t=3.
figure(3)
plot(T,Y(:,1),[0,4],[-5,-5],'k')

% Calculamos el tiempo del evento mediante la opci�n Events

opciones=odeset('RelTol',1e-8,'Events',@evento);
[T1,Y1,te,ye,ie]=ode45(F,tspan,Y0,opciones);
te
ye
ie

% te =  3.19145847856512% Tiempo del evento que se corresponde con lo 
% % observado gr�ficamente.
% 
% 
% ye = -5  -1.99325039067065 % Observa que y1(te)=-5
% 
% 
% ie = 1  % Evento encontrado

function [mifun,isterminal,direction]=evento(t,y)
mifun=y(1)+5;
isterminal=0;
direction=0;
end
