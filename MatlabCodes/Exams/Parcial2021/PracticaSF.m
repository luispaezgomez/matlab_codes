%       ! 1 0<=x<pi
% x(t)= !
%       ! -1 pi<=x<2*pi
clear all
clc
t=0:0.001:2*pi;
xt=(t>=0)+(t>=pi).*-2;
plot(t,xt,'b','LineWidth',1.5)
ax=gca;
ax.XAxisLocation='origin';
ax.YAxisLocation='origin';
hold on
grid on
% SF
k=100; % iteraciones
s=0; % suma
for n=1:k
    p=(2/(n*pi))*(1-(-1)^n).*sin(n*t);
    s=s+p;
    if (n==k)
    plot(t,s,'g')
    pause(1)
    end
end