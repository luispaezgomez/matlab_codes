format short g
n=4;
A=magic(n);
A(2,2)=6;
deter = det(A);
b = [19 15 15 19]';
SolucionReal = [1 0 1 0]';
matAmpliada =[A [19;15;15;19]];
RankSistema=rank(A)-rank(matAmpliada); % Si es 0 es un SCD
xex = A\b;
% Primera parte, ahora pivoteo LU
[L,U,P]= lu(A);
x = U\(L\(P*b));
% Calculamos el error absoluto Ea = abs (xr-xi)
Ea =abs(x-SolucionReal);

