f = @(x)sin(x.^2);
x=[0:0.001:pi];
plot (x,f(x));
ax=gca;
ax.XAxisLocation='origin';
ax.YAxisLocation='origin';
% Hay que hallar primero el cero por el metodo de newton
x1=1.5;
tol=10^-12;
[xcero,it]=newton_tol(f,x1,tol);
integral(f,0,xcero);