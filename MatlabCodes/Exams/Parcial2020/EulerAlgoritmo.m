function [T,Y]=EulerAlgoritmo(f,t0,tf,y0,h)
T=(t0:h:tf)';
N=length(T);
Y=zeros(N,length(y0));
y0=y0(:)';Y(1,:)=y0;
for j=1:N-1
    Y(j+1,:)=Y(j,:)+h*f(T(j),Y(j,:))';
end
return