function [xcero,it]=newton_tol(f,x1,tol) 
% funcion que calcula un cero de f
% por el metodo de newton con
% un error < tol
x(1)=x1; % valor inicial
h=1e-5; % para la derivada
err=2*tol; % valor inicial grande
k=1;
while( (err > tol) && ( k < 100 ) )
    fp = (f(x(k)+h)-f(x(k)-h))/(2*h); % estimacion de la derivada
    x(k+1)=x(k)-f(x(k))/fp;
    err=abs(x(k+1)-x(k))/abs(x(k+1));
    k=k+1;
end
xcero=x(end); % ultimo valor de x
it=k-1;
return

