n=5; % Numero de nodos
% SIHAY MUCHOS NODOS, FENOMENO DE RUNGE
% N+1 Valores pq son los coeficientes de un polinomio de grado N
% N+1 incognitas
% N=2 SIGNIFICA QUE EL POLINOMIO DE INTERPOLACION ES DE GRADO UNO
f = @(x)sin(x.^2);
nodos = linspace (0,pi,n);
valint = zeros (1,n);
 for k=1:n
     valint(1,k)=f(nodos(k));
 end
N = length(nodos)-1;
% Tenemos los nodos evaluados en la funcion
coef = polyfit (nodos,valint,length(nodos)-1) % de grado n
sparse(coef)
% En pol estan los coeficientes 
mallado = linspace(min(nodos),max(nodos));
% Cuando no pones nada al final va a sacar 100
yy = polyval(coef,mallado);
% En polyval se evaluan los coeficientes hallados en pol
plot(nodos,valint,'.','MarkerSize',20)
hold on
x = [0:0.001:pi];
plot(mallado,yy,x,f(x))

% nodos = [x1,x2,x3...]
% valint = [y1,y2,y3...]
%y = polyval (c,x) -> evalua el polinomio de coeficientes c en un punto x o una particion x
% cuando hay muchos nodos y valint se produce el fenomeno de runge
% N <length (nodos) -1 ---> polinomio de ajuste
% N =length (nodos) -1 ---> polinomio de interpolacion
% los dos se calculan con polyfit
