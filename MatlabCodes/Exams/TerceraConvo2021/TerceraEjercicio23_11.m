format long g
f = @(t,y)[y(2);-2*y(2)-2*y(1)+4*cos(t)+2*sin(t)];
y0=[0;-3];
t0=0; tf=5*pi;
tspan=[t0,tf]; %%[t0,1.5,tf] %%intervalo valores que quiero, si pones poner mas de 2 solo te da esos puntos
tol = 10e-8;
opts = odeset('RelTol',tol,'AbsTol',tol);
[T,Y]=ode45(f,tspan,y0,opts);
plot(T,Y(:,1),'b',T,Y(:,2),'k') %%grafica
ax=gca;
ax.XAxisLocation='origin';
ax.YAxisLocation='origin';
[T,Y]=odesistemaseventos23_11;
% solucion
g = @(t)[-5*exp(-t)*sin(t)+2*sin(t)];
% for para calcular el error abs maximo
error =0;
k=1;
while (T(k)<tf)
    error = g(T(k))-T(k);
    if (error>ans)
    error = ans;
    end
    k=k+1;
end
abs(error)
%% ERROR?

