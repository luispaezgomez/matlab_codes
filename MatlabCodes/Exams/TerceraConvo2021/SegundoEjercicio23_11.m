f = @(x)sin(x-3)+3;
g = @(x)0.29*x+1.55;
h = @(x)sin(x-3)+3-(0.29*x+1.55);
x=[0:0.01:7];
plot(x,f(x),'r',x,g(x),'k',x,h(x),'y')
ax=gca;
ax.XAxisLocation='origin';
ax.YAxisLocation='origin';
x0=1;
tol=10^-10;
maxiter=7;
% La funcion a la que le queremos hallar cero es
% fun = sin(x-3)+3-(0.29*x+1.55)
[sol,it,error,valor,condicion]=newton23_11(x0,tol,maxiter)
% Con la funcion newton falla para la ultima x0
% y con fzero?
x00=1;
solfzero=fzero(h,x00);
area=integral(h,1.60,5.88)

y=0;
z=0;
format long
for x=0:0.000001:8
    if(sin(x-3)+3>y) %% funcion a la que le quiero calcula el max
    y=sin(x-3)+3;
    z=x;
    end
end
disp(z)
