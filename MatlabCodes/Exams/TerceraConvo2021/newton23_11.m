%%modifica la funcion y la dev tonto
function [sol,it,error,valor,condicion]=newton23_11(x0,tol,maxiter)
it=0;
error=tol+1;
x1=x0;
while error>=tol && it<=maxiter
    x2=x1-(sin(x1-3)+3-(0.29*x1+1.55))/(cos(x1)-0.29); %% arriba original
    error=abs(x1-x2);                                  %% abajo derivada
    x1=x2;                                             %% con x1
    it=it+1;
end
sol=x2;
valor=sin(x2-3)+3-(0.29*x2+1.55); %%aqui original con x2
condicion=(cos(x2)-0.29);  %%aqui derivada con x2
% PON LO MISMO QUE ARRIBA PERO SUSTITUYENDO X1 POOR X2
return