function [T,Y]=odesistemaseventos23_11
f = @(t,y)[y(2);-2*y(2)-2*y(1)+4*cos(t)+2*sin(t)];  %cambia la funcion tontito es lo que esta igualado a la derivada de la primera variable 
y0=[-3;0]; %cond iniciales % y la otra la de la segunda y(1)=1ºvariable e y(2)=2ºvariable
t0=0; tf=5*pi;
tspan=[t0,tf];
tol=10e-8;
opts = odeset('Events',@evento,'RelTol',tol,'AbsTol',tol); %%cambiar en eventos la funcion
[T,Y,te,ye,ie]=ode45(f,tspan,y0,opts);
format short g
te
ye
ie