f=@(x)x.*sin(1./x);
x=[0:1e-4:1];
plot (x,f(x),'r');
ax=gca;
ax.XAxisLocation='origin';
ax.YAxisLocation='origin';
% 0.30, 0.14,0.10
x0=0.1;
tol=10^-10;
maxiter=4;
[sol,it,error,valor,condicion]=newton8_09_2019(x0,tol,maxiter)

