%%modifica la funcion y la dev tonto
function [sol,it,error,valor,condicion]=newton8_09_2019(x0,tol,maxiter)
it=0;
error=tol+1;
x1=x0;
while error>=tol && it<=maxiter
    x2=x1-((x1.*sin(1./x1))/(sin(1./x1)-cos(1./x1)./x1)); %% arriba original
    error=abs(x1-x2);                                  %% abajo derivada
    x1=x2;                                             %% con x1
    it=it+1;
end
sol=x2;
valor=x2.*sin(1./x2); %%aqui original con x2
condicion=(sin(1./x2)-cos(1./x2)./x2);  %%aqui derivada con x2
return