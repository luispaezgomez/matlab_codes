% N <length (nodos) -1 ---> polinomio de ajuste
% N =length (nodos) -1 ---> polinomio de interpolacion
format short g
n=13;
f=@(x)sin(4*(log(x+1))); 
nodos=linspace(0,9,n);
y = zeros (1,n);
 for k=1:n
     y(1,k)=f(nodos(k));
 end
% Tenemos los nodos evaluados en la funcion
pol = polyfit (nodos,y,length(nodos)-1); % de grado n-1
% En pol estan los coeficientes 
xx = linspace(min(nodos),max(nodos));
% Cuando no pones nada al final va a sacar 100
yy = polyval(pol,xx);
% En polyval se evaluan los coeficientes hallados en pol
plot(nodos,y,'.','MarkerSize',20)

hold on
c = length(nodos)-1
title (['Grado Polinomio: ',num2str(length(nodos)-1)]);
minx = 1.15*min(x); 
maxx = 1.15*max(x);
miny = 1.15*min(f(x)); 
maxy = 1.15*max(f(x));
plot([minx,maxx],[0,0],'k','LineWidth',2);
plot([0,0],[miny,maxy],'k','LineWidth',2);
plot(maxx,0,'>k','markersize',10,'MarkerFaceColor','k');
plot(0,maxy,'^k','markersize',10,'MarkerFaceColor','k');
text(maxx,0.3,'X','fontsize',15,'color','k');
text(-0.7,maxy,'Y','fontsize',15,'color','k');
plot (xx,yy) % polinomio de interpolacion

x=[0:0.001:9];
plot (x,f(x))
error=yy(2)-f(2)
error=yy(6)-f(6)

