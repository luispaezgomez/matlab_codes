% DIBUJAR ELIPSE
x = [2-3^1/2:0.01:2+3^1/2];
j = @(x)1+(2-2/3*(x-2).^2).^1/2 %  Ecuacion despejada de la elipse
x = [2-3^1/2:0.01:2+3^1/2];
g = @(x)1-(2-2/3*(x-2).^2).^1/2
h = @(x)x.^2-3*x+2;


t = -pi:0.01:pi;
a = 3^1/2;
b = 2^1/2;
x1 = 2;
y1 = 1;
x=x1+a*cos(t);
y=y1+b*sin(t);
plot (x,y)
hold on
x = [-4:0.001:4];
plot (x,x.^2-3*x+2)


f = @(x)1+(2-2/3*(x-2).^2).^1/2-(x.^2-3*x+2);
df= @(x)(4/3*(x-2)-2/3*(2*x-4))/2*sqrt(2-2/3*(x-2).^2)-(2*x-3)  % derivada analítica

x0=0;   %probar con distintas x0= -5, -1.5, 0, 20
niter=10;
x1b=newtonescalar(f,df,x0,niter)
%SOLO CALCULA LA INTERSECCION CON LA POSITIVA

function x=newtonescalar(fun,dfun,x0,numiter)
for k = 1:numiter
    d = fun(x0)/dfun(x0);
    x = x0-d;
    fprintf('x(%2d)=%16.15f \n',k,x)   
    pause(0.1)  
    x0 = x;
end
end
