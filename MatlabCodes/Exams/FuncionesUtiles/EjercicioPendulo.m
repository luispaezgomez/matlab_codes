% definir una funcion de eventos
%deteccion de eventos
L=2; 
f = @(t,y) [y(2);-9.8*sin(y(1))/L];
   
% f (funcion vectorial) depende de y y t (la t no se utiliza pero...
%hay que ponerlo para que funcione ODE45

y0 = [0.2 0]; %la condicion inicial tiene que ser un vector
y0p =0;
tspan = [0 3 pi 4 10];
[t,y]= ode45 (f, tspan , y0)
[t y]
% y en t=Pi es 
ypi = y(3,:)
f (t(3),ypi')
% y'' en t=pi  -9,8*sin (ypi(1))/L

%eventos y=0
tspan = [0 10];
%activamos lla deteccion de eventos con la funcion  @eventos
tol =1e-10;
opt=odeset ('Events',@eventos,'AbsTol',tol,'RelTol',tol);
[t,y,te,ye,ie] = ode45 (f,tspan,y0,opt)
[t y]
[te ye ie]
plot (t,y)
hold on 
plot (te,0,'sk')
hold off
axis

function [valor,isterm,dir]=eventos (t,y)
%entrada t,y -> function para monitorizar el evento definido en valor
valor = y(2); % marca los pasos por y=0
dir=1; %direccion -1,-0,1
isterm =0; %no se para
end
% para que valor de y coincide la velocidad con la posicion
% es decir cuando se cortan la grafica azul y la roja y(1)-y(2)
%diff (te) es el periodo con dir 1 y isterm 0