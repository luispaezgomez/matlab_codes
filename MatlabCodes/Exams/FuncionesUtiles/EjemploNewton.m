% Ejemplo aplicación del Método de Newton 
% función anónima
f = @(x) log(x.^4 + x.^2 +2) .* exp(-x) - 5
fplot(f,[-2 2])

df= @(x) exp(-x).*((4*x.^3+2*x)/(x.^4 + x.^2 +2) ...
    - log(x.^4 + x.^2 +2) ); % derivada analítica

x0=20;   %probar con distintas x0= -5, -1.5, 0, 20
niter=50;
x1b=newtonescalar(f,df,x0,niter)


%%
function x=newtonescalar(fun,dfun,x0,numiter)
% método de Newton para ecuaciones escalares:
% fun y dfun son la función y su derivada,
% x0 es la estimación inicial para el cero.
% El criterio de parada es el número de iteraciones,
% no hace estimación del error
for k = 1:numiter
    d = fun(x0)/dfun(x0);
    x = x0-d;
    fprintf('x(%2d)=%16.15f \n',k,x)  % Write data to text file
      %   %d: Notación decimal (en base diez) para enteros (con signo).
      %   %2d  2 cifras
      %   aplicable al primer argumento, k
      %   %f: Notación decimal con punto fijo.
      %   %16.15f  16 cifras, 15 decimales
      %   aplicable al segundo argumento, x
      %   \n: salto de línea.
    pause(0.1)  % pausa para ir viendo la iteración en pantalla
    x0 = x;
end
end