clc,clear
f = @(t,y)[y(2);-y(1)];
y0=[1 0]';
tspan = [0 14];
opciones = odeset ('Events',@evento);
[T,Y,te,ye,ie]=ode45(f,tspan,y0,opciones);
format short g
te 
ye
ie
format

% En el examen tengo que cambiar:
%        - f = @(t,y)....
%         - yo = [.....]
%        - La funcion eventos
