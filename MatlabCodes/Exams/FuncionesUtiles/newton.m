function [sol,it,error,valor,condicion]=newton(x0,tol,maxiter)
it=0;
error=tol+1;
x1=x0;
while error>=tol && it<=maxiter
    x2=x1-(sin(x1-3)+3-0.29*x1-1.55)/(cos(x1-3)-0.29);
    error=abs(x1-x2);
    x1=x2;
    it=it+1;
end
sol=x2;
valor=sin(x2-3)+3-0.29*x2-1.55;
condicion=cos(x2-3)-0.29;
return