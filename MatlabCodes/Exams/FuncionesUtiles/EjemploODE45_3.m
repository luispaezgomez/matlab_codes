%ECUACION PENDULO
clc,clear
% SIEMPRE CON PUNTOS EN DECIMALES POR FAVOR
%f = @(t,y)[-y(1)+6*y(2);-9*y(1)-2*y(2)];
f = @(t,y)[y(2);-9.8*y(1)];
y0=[pi/2; 0];
tspan=[0,20];
[T,Y]=ode45 (f,tspan,y0)
figure(1) % graficas de y1(t( y y2(t)
plot (T,Y(:,1),'b',T,Y(:,2),'k')
figure(2) % grafica de curva parametrica
plot (Y(:,1),Y(:,2),y0(1),y0(2),'or')
% FIGURE 2 ES ORBITA DEL SISTEMA
figure(3) % como ode45 ha elegido los pasos de la particion T
plot (1:(length(T)-1),diff(T))