function InterpolarSplineFijarBordes
x=[0:0.4:2];
y = x.^2+rand(1,length(x));
nodos=x;
valint=y;
xx=[0:0.01:2];
der1=(y(2)-y(1)/x(2)-x(1));
der2=(y(end)-y(end-1)/x(end)-x(end-1))
yy=spline(nodos,[der1,valint,der2],xx);
plot(nodos,valint,'ob',xx,yy)
end