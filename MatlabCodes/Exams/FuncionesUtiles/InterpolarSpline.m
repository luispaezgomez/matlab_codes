function InterpolarSpline
x=[0:1.8:9];
y=sin(4*log(x+1));
nodos=x;
valint=y;
xx=[0:0.01:9];
yy=spline(nodos,valint,xx);
% se le meten los puntos, la funcion y un intervalo pequeño
plot(nodos,valint,'ob',xx,yy)
% se representan los puntos y el spline
end