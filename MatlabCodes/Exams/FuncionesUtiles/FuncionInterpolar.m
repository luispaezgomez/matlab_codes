function interpolarpol % Usar para pocos puntos
x=[0:1.8:9];
y=sin(4*log(x+1)); % cambiar funcion
nodos = x;
valint = y;
N = length(nodos-1);
coef = polyfit (nodos,valint,N);
xx = [0:0.01:9]; % cambiar el rango
yy = polyval (coef,xx)
plot (nodos,valint,'ob',xx,yy)

mipolint = @(x)(polyval(coef,x)); % calcular, integra, definir primero la funcion

Q = integral ( mipolint,0,9);
z = polyval(coef,2)-sin(4*log(2+1))
o = polyval (coef,6)-sin(4*log(6+1)) 
% errores
end
% para los nodos dividir el valor entre nodos-1, si nodos es 6
% dividimos 9 entre 5