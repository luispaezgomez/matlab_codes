%  intro a Matlab y Analisis numerico
%   
%  jgv: Octubre 2018
%
%  son algunos ejemplos sencillos
%  no se trata de ser exhaustivo, mas adelante
%  se explicaran algunos comandos
%  
%  Animar a los alumnos a experimentar
%  emplear el tiempo que se crea necesario
%  en clase pero no debe asustar.
%  en pocas practicas se volvera "natural".
%
%  Si os parece bien podemos subir este fichero
%  con las mejoras que propongais a EV 
%
%  Modo de empleo:
%    - escribir linea a linea en la ventana de comandos
%    - ir colocando la orden break detras de cada 
%      comando o grupo de comandos
%    - lo mismo con pause
%
%  Esta pensado para impartir DOS clases
%  con ejercicios, variantes y resolviendo dudas.

pi  % usando Matlab como calculadora

format long g  % mas decimales en pantalla.
               % NO AFECTA la precision del calculo
pi

help format  % ver doc y help de matlab

% con cuantos digitos trabaja Matlab?
% limite de la precision?  
% lo veremos al calcular eps: numero de maquina

(4/3-1)*3-1  % que ha "fallado"?

realmax   

realmax*10

realmin

1/0

0/0

sqrt(-16)  % matlab sabe trabajar con complejos

% un poco de operadores logicos
% para usar en los if y while

1>2

2>1

1 == 4-3  % equal to

2 ~= sqrt(4)  % not equal (diferente de )

1 < 2 && 3 > 2  % operador AND

1 < 2 || 3 < 2  % operador OR

% vectores

v=[1 2 3 4]  % vector fila

size(v)

v'  % traspuesto; vector columna

% oepraciones vectoriales

v*2
v+2  % ojo: no es lo mismo que mat I

v*v' %  producto vectorial: fila x columna

% v^2  %  ERROR

v.^2  % Operador vectorial: COMPONENTE A COMPONENTE
      % enfasis. se utiliza continuamente y no
      % es intuitivo.

w=[1:6] % BUCLE IMPICITO se utiliza MUCHO

w(1:3)  % submatriz

w(4:-1:2) % submatriz de dcha a izda

A=magic(4)

Acol1= A(:,1)  % todos los elementos de la columna 1

Afila3= A(3,:) % fila 3

Asubmatriz= A(2:3,2:3)

% Para que sirve esto ?

%
% ejemplo sencillo: plot del sin(x)
%
% En Matlab TODO son matrices.

x=[0:0.01:2*pi]; % ; suprime la salida por pantalla

plot(x,sin(x))   % sin es una funcion interna de Matlab

Q=quad(@sin,0,pi) % Matlab "sabe" integrar
                % @ es un "handle"; una forma de
                % invocar a la funcion sin(x)

                
err1=Q-2 % error absoluto cometido CON SIGNO

err2=abs(Q-2) % error absolutoo SIN signo

err3= abs(Q-2)/2  % error realtivo: cifras significativas
                  % en este ejmplo conocemos
                  % 8 cifras de la cuadratura
% en numerico en general no conocemos el resultado
% exacto y debemos estimar el error por
% otro procedimiento.



% Funciones:  
% Apartado clave en Matlab; insistir a los alumnos
%  hay varias formas de definir funciones en Matlab
%  Usaremos SOLO dos de ellas
%  funciones en ficheros  (ver fun1.m)
%  funciones anonimas  (comand @ )

type fun1.m  % para "ver" el listado de fun1.m

help fun1

x=[-4:0.01:4];
plot(x,fun1(x))

% ejercicio
% cuanto vale el area encerrada por fun1 entre 4 y 4?

Q=quad(@fun1,-4,4)

Qex=2*atan(4)

err=abs(Q-Qex)/Qex  % veremos mas en Practica de Quad

%  Funciones anonimas (opcion avanzada de Matlab que
%                      presenta ventajas (rapidez y flexibilidad)

fun2 = @ (x) 1+x.^2

x=[-4:0.01:4];
plot(x,fun2(x))

Q2=quad(fun2 ,-4,4)  %  OJO ahora no ponemos el @
                     %  fun2 YA es un handle !!
                     
% NOTA: las funciones suelen provocar muchos errores
%       hay que saber como definirlas, invocarlas
%       y pasarselas como argumentos a otras funciones
%       recomendar: precaucion y practuca




% acabar con el ejercicio de obtener el numero de maquina
% explicando el bucle for, el comando if y el while

1+1 == 1   % sabe sumar Matlab?

1+1e-20 == 1  % no del todo bien? que ha pasado?

% 1e-20 es "demasiado peque?o" comparado con 1
% y Matlab lo trata como cero. 
% estamos mas alla del limite de la precision de nuestra
% calcualdora. (doble precision)

% PREGUNTA: ?cual es el numero mas peque?o
%           que al sumarselo a 1 con  Matlab, 
%           este lo distingue de 1?
% 
% Esa es la definicion de NUMERO DE MAQUINA

% Primera aproximacion: bucle for

format short g % volvemos a formato short para
               % mejorar la salida de pantalla

x=1;
for n=1:20
    1+x == 1
    x=x/2  % probar con /2
end

% Resultado:  x= 1e-16
% el formato de salida es incomodo
% lo mejoramos con una MATRIZ de salida o lista

lista1=[ ]; % matriz vacia
x=1;
for n=1:60
    flag = 1+x == 1; % el ; suprime la salida pantalla
    lista1=[lista1; n flag x]; % explicar el comando
    x=x/2;                   % ; (nueva linea)
end                         
lista1     % algo mejor


% Segunda aproximacion: paramos cuando llegemos al
%                       numero de maquina


lista2=[ ];
x=1;
for n=1:60
    flag = (1+x == 1) ; % cierto (1) o falso (0) 
    if (flag == 0)  % OJO == , no =  Operador logico
        lista2=[lista2; n flag x]; 
    else
        break
    end
    x=x/2;
end                          
lista2   
    
% no hacemos calculos inutiles y paramos 
% cuando se consigue el objetivo

% Tercera aproximacion:  comando while

lista3=[ ];
x=1;
n=1;  % inicializamos el contador de iteraciones
while ( x+1 ~= 1)  %  OJO usamos el DISTINTO a 
    flag= x+1==1;
    lista3=[lista3; n flag x];
    x=x/2;
    n=n+1;
end
lista3

% COMENTARIOS
%
%  - este fichero es un curso acelerado
%    de programacion. Los alumnos saben
%    programar de primero y deben trabajar MUCHO
%    por su cuenta.
%  
%  - es facil cometer errores en los while y
%    meternos en bucles infinitos
%    la ejecucion se interrumpe con ctr-c
%    podemos usar el contador de iteraciones
%    para interrumpir la ejecucion cuando se
%    alcanza un numero maximo de iteraciones
%
%    while( (x+1 ~= 1) && (n <= 100) )
%
%    end
%
%  - NOTAS numericas:
%    podemos ir mencionando comentarios sobre
%    errores, convergencia, memoria, 
%    tiempo de ejecucion a lo largo de los ejemplos
%    todo esto aprecera durante el curso.
%
%    comandos who, whos, tic, toc ...
