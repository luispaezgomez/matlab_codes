% ejercicio Fibonacci 11.1
% x(n+1) = x(n)+x(n-1)
%
x(1)=1;
x(2)=1;

% solucion por FUERZA BRUTA
% 
% nmax=100;
% for n=3:nmax
%     x(n)=x(n-1)+x(n-2);
%     if(x(n) > 1e6)
%         return
%     end
% end

% solucion INTELIGENTE
% con un while

y(1)=1;
y(2)=1;
n=3; % valores iniciales
while (y(n-1)+y(n-2) < 1e6 )
    y(n)=y(n-1)+y(n-2);
    n=n+1;
end

y(n)=y(n-1)+y(n-2);
[n y(n)]



