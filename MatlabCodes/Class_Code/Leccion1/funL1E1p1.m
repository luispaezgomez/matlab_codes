
function A = funL1E1p1(n)
% funcion Ejercicio 9.1
% L1E9p1_matriz
% INPUT -> n dimension de la matriz
% OUTPUT -> matriz A

for i=1:n % bucle por filas
    for j=1:n % bucle por columnas
        A(i,j) = j*i^2;
    end
end




end
