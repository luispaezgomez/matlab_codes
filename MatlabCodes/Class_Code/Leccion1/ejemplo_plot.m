% fichero de ejemplo
% dibujamos la funcion
% f(x) = sin(x^2)

x = 0:0.01:2; % vector de abcisa

% f es una funcion en fichero

% funcion ANONIMA 
g = @(x) 2-x.^2;

% funcion h(x) de tipo file pero
% en el mismo script

plot(x,f(x),...
    x,g(x),...
    x,h(x)) % plot

function y = h(x)
% ejeplo de funcion file
% en el mismo script
y = x.*log(x);
end
