function out = funL1e10p1(x)
% funcion signo con un if logico
%
% NOTA: esta funcion No TRABAJA
%       VECTORIALMENTE !!!
%
if ( x > 0)
    out = 1;
elseif (x < 0)
    out = -1;
else
    out = 0;
end

end