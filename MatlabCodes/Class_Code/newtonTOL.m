function [sol,indicador,iter]=newtonTOL(f,fp,x0,TOLr,TOLa,maxiter)
% NEWTON - Método de Newton para resolver la ecuación f(x)=0
%
%   FORMATO: [sol,indicador,iter]=newton(f,x0,TOLr,TOLa,maxiter)
%
%   f - función de Matlab a la que queremos calcular un cero
%   fp - derivada de la función f  
%   x0 - Aproximación inicial de la solución.
%   TOLr - Tolerancia relativa. 
%   TOLa - Tolerancia absoluta. 
%   maxiter - Número máximo de iteraciones
%
%   sol - Aproximación de la solución
%   indicador - Vale 0 si el método de Newton converge y 1 en caso contrario.
%   iter - Número de iteraciones realizadas.

iter=0; % Inicializamos el contador de iteraciones

x=x0; % Punto inicial

estim_error=max(TOLr*abs(x),TOLa)+1; % Estimación inicial del error

while iter<maxiter && estim_error>max(TOLr*abs(x),TOLa)
    iter=iter+1;   % Incrementamos el contador de iteraciones 
    e = -f(x)/fp(x);
    x = x + e      % Calculamos el siguiente punto
    estim_error=abs(e); % Estimamos el error
end

sol=x; % Salimos del bucle while cuando sobrepasemos el número máximo de 
       % iteraciones o cuando el error estimado cumpla los criterios de 
       % tolerancia pedidos

if estim_error>max(TOLr*abs(x),TOLa)
    indicador=1;
else
    indicador=0;
end