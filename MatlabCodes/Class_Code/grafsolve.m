% Fichero de instrucciones: grafsolve.m 
x=-1:0.01:1;
% [X,Y] = meshgrid(x,y) returns 2-D grid coordinates based 
%   on the coordinates contained in vectors x and y. 
%   X is a matrix where each row is a copy of x, and 
%   Y is a matrix where each column is a copy of y. 
%   The grid represented by the coordinates X and Y has 
%   length(y) rows and length(x) columns.
% [X,Y] = meshgrid(x) is the same as [X,Y] = meshgrid(x,x), returning
%   square grid coordinates with grid sizelength(x)-by-length(x).
[X,Y]=meshgrid(x);
Z1=2*X-Y-exp(-X);
Z2=-X+2*Y-exp(-Y); 
% contour(X,Y,Z,levels) 
%  specifies the contour lines to display as the last argument. 
%  Specify levels as a scalar value n to display the contour lines 
%  at n automatically chosen levels (heights). 
%  To draw the contour lines at specific heights, specify levels 
%  as a vector of monotonically increasing values. 
%  To draw the contours at one height (k), 
%  specify levels as a two-element row vector [k k].
figure(1)
contour(X,Y,Z1,[0,0],'b','Linewidth',2) 
axis equal
hold on 
contour(X,Y,Z2,[0,0],'r','Linewidth',2) 
hold off
shg

n=5   % número de curvas de nivel que queremos que aparezcan en el dibujo
figure(2)    % 'ShowText','on': indica los valores dibujados
contour(X,Y,Z1,n,'b','Linewidth',2,'ShowText','on') 
axis equal
hold on 
contour(X,Y,Z2,n,'r','Linewidth',2,'ShowText','on') 
hold off
shg

v=[-2 -1 0 1 2]  % valores elegidos para las curvas de nivel
figure(3)        % 'ShowText','on': indica los valores dibujados
contour(X,Y,Z1,v,'b','Linewidth',2,'ShowText','on') 
axis equal
hold on 
contour(X,Y,Z2,v,'r','Linewidth',2,'ShowText','on') 
hold off
shg
