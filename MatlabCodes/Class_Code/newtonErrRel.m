function sol = newtonErrRel(fun,x0,tol)
% algoritmo de Newton
err = 2*tol; % estimación inicial del error: al menos 1 iteración
it=0; lista=[ ];
xk=x0; h=1e-5;
while( err > tol)
    fp = (fun(xk+h)-fun(xk))/h; % derivada numérica
    % alternativa -> función derivada analítica
    xkplus1=xk-fun(xk)/fp;  % iteración de Newton
    it=it+1; % contador de iteraciones
    % nueva estimación del error RELATIVO
    err=abs(xkplus1-xk)/abs(xk); % if xk ~= 0
    lista=[lista;xk it err]; % para comprobar
    xk=xkplus1; % actualizamos xk
end
sol = xkplus1;
lista
return