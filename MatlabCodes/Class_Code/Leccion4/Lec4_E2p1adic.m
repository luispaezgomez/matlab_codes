% Lección 4, ejercicio 2.1 adicional
% resolución del PVI
%      u'=1+u^2v-4u,  v'=3u-u^2v,    u(0)=1.5, v(0)=3
% mediante RK clásico (orden 4)
%
f = @(t,y) [1 + y(1).^2.*y(2) - 4*y(1);...% primera ecuación del sist.
             3*y(1) - y(1).^2.*y(2)];  % segunda ecuación del sist.
y0= [1.5; 3];   % condición inicial
t0=0;
tf=2;
h=0.01;
[T,sol]=rk4(f,t0,tf,y0,h);  % integramos mediante RK4
u=sol(:,1);
v=sol(:,2);

plot(T,u,'r',T,v,'b','Linewidth',2)
xlabel('t')
ylabel('u,v')
set(gca,'FontSize',18)
legend('u(t)','v(t)','Location','Best')

%% Método de Runge-Kutta clásico (ORDEN 4) en DIM = m 
function [t,y] = rk4(f,t0,tf,y0,h) 
m = length(y0); % orden del problema
t = [t0:h:tf]'; % vector de tiempos en COLUMNA
niter = length(t)-1; % número de iteraciones
y = zeros(niter+1,m); % salida en forma de matriz con m columnas
%
% Se OPERA por COLUMNAS
% se guardan los resultados por FILAS
%
y(1,:) = y0' ; % almacenamos el valor inicial como FILA
% notación y0 = y(n)  e y1 = y(n+1), y0 e y1 vectores auxiliares
for n = 1:niter
    k1 = f(t(n),y0);  % operación por COLUMNAS
    k2 = f(t(n)+0.5*h,y0+0.5*h*k1);
    k3 = f(t(n)+0.5*h,y0+0.5*h*k2);
    k4 = f(t(n)+h,y0+h*k3);  
    y1 = y0+h*(k1+2*k2+2*k3+k4)/6; % y1 es COLUMNA
    y(n+1,:)= y1'; % almacenamos por FILAS
    y0 = y1; % actualizamos para la siguiente iteración
end
end