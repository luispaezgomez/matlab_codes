% Lección 4, ejercicio 4.1, método de Euler
% Resolución del PVI y'-y=tcos(t), y(0)=0

h=0.01;  % paso de integración
E1p1(h)

%
function E1p1(h)
[T,sol_euler]=eul(@fun,0,1,0,h); 
sol_ex=(1/2)*(sin(T)-T.*cos(T)+T.*sin(T)); % solución exacta PVI
plot(T,sol_ex,'b',T,sol_euler,'r--','Linewidth',2)
xlabel('t')
ylabel('y')
set(gca,'FontSize',18)
title(['h=',num2str(h)])
legend('Exacta','Euler','Location','NorthWest')
shg
end
%

function dydt=fun(t,y)   % miembro derecho de la ec. y'=f(t,y)
dydt=y+t.*cos(t);
end

%%
% argumentos de entrada: 
%   f: miembro derecho de la ecuación y'=f(t,y)
%   intervalo integración [t0,tf]
%   y0: condición inicial, y(t0)=y0
%   h: paso de integración
% argumentos de salida:
%   T: vector con los valores de t en los que queremos aproximar
%      la solución
%   Y: si se trata de una ecuación, vector que contiene a las 
%       aproximaciones de la solución; 
%      si se trata de un sistema de n ecuaciones, matriz que en 
%       cada columna contiene las aproximaciones de cada componente
%       de la solución 
function [T,Y]=eul(f,t0,tf,y0,h) 
T=(t0:h:tf)';  % vector con los valores de t en los que 
               %   se aproxima la solución
N=length(T);
Y=zeros(N,length(y0)); % matriz con N filas y n columnas (siendo n
                       %   el número de componentes de y0)
y0=y0(:)';
Y(1,:)=y0;
for j=1:N-1                            
   Y(j+1,:)=Y(j,:)+h*f(T(j),Y(j,:))';  
end
end