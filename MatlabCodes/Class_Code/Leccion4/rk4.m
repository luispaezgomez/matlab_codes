
%% Método de Ruge Kutta clásico en DIM = m  ORDEN 4
function [t,y] = rk4(f,t0,tf,y0,h) 
m = length(y0); % orden del problema
t = [t0:h:tf]'; % vector de tiempos en COLUMNA
niter = length(t)-1; % número de iteraciones
y = zeros(niter+1,m); % salida en forma de matriz con m columnas
%
% Se OPERA por COLUMNAS
% se guardan los resultados por FILAS
%
y(1,:) = y0' ; % almacenamos el valor inicial como FILA
% notación y0 = y(n)  e y1 = y(n+1), y0 e y1 vectores auxiliares
for n = 1:niter
    k1 = f(t(n),y0);  % operación por COLUMNAS
    k2 = f(t(n)+0.5*h,y0+0.5*h*k1);
    k3 = f(t(n)+0.5*h,y0+0.5*h*k2);
    k4 = f(t(n)+h,y0+h*k3);  
    y1 = y0+h*(k1+2*k2+2*k3+k4)/6; % y1 es COLUMNA
    y(n+1,:)= y1'; % almacenamos por FILAS
    y0 = y1; % actualizamos para la siguiente iteración
end
end