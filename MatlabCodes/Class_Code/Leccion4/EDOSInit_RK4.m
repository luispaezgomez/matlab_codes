%EDO
% resolución del PVI  y'-y=t, y(0)=1
%% PRE
close all;clear variables;
format short g
f = @(t,y) (y+t);  % y'=f(t,y), miembro derecho de la ecuación
solu = @(t) 2*exp(t)-(1+t);
y0=1;
t0=0;
tf=10;
%% SOLVER
h=0.01;
t = t0:h:tf;
tspan=[t0 tf];
%options=odeset('RelTol',1e-9,'AbsTol',1e-12)
sol=ode45(f,tspan,y0)
%Ver estructura sol (Clase 2)
%Euler
[T,Y]=euler(f,tspan,y0,h);
%Euler Mejorado
[Tm,Ym]=eulermejorado(f,tspan,y0,h);
%RK4
[Trk4,Yrk4]=RK4(f,tspan,y0,h);
%% POST
figure(1)
plot(sol.x,sol.y,t,solu(t),T,Y,Tm,Ym,Trk4,Yrk4,'Linewidth',2)
xlabel('t')
ylabel('y')
set(gca,'FontSize',18)
%plot(sol.x,sol.y,t,solu(t),T,Y)
legend('ode45','Analítica','Euler','Euler Mejorado','RK4','Location','Best')

%% Método Euler
function [t,y]=euler(f,tspan,y0,h)
% metodo de euler en 1D
t=[tspan(1):h:tspan(2)]';%mallado \Delta
n=(tspan(2)-tspan(1))/h;
y=zeros(n+1,1); % inicializamos a cero
y(1)=y0;
for k=1:n
    y(k+1)=y(k)+h*f(t(k),y(k));
end
end

%% Método Euler Mejorado
function [t,y]=eulermejorado(f,tspan,y0,h)
% metodo de euler mejorado en 1D
t=[tspan(1):h:tspan(2)]';
n=(tspan(2)-tspan(1))/h;
y=zeros(n+1,1); % inicializamos a cero
y(1)=y0;
for k=1:n
    k1=f(t(k),y(k));
    k2=f(t(k)+h/2,y(k)+0.5*h*k1);
    y(k+1)=y(k)+h*k2;
end
end

%% metodo RK4 1D
function [t,y] = RK4(f,tspan,y0,h)
t=[tspan(1):h:tspan(2)]';%malla de paso constante
n=round((tspan(2)-tspan(1))/h);
y=zeros(n+1,1); % inicializamos a cero vector y
y(1)=y0;
for k=1:n
    k1=f(t(k),y(k));
    k2=f(t(k)+h/2,y(k)+0.5*h*k1);
    k3=f(t(k)+h/2,y(k)+0.5*h*k2);
    k4=f(t(k)+h,y(k)+h*k3);
    y(k+1)=y(k)+h*((1/6)*k1+(2/6)*k2+(2/6)*k3+(1/6)*k4);
end
end