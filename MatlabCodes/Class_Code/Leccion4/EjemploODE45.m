clc,clear
f = @(t,y)-y+t; %Sale del RHS de la EDO
y0=1;
tspan = [0,3]; % extremos del intervalo donde queremos la solucion
[T,Y]=ode45(f,tspan,y0);
% La solucion simbólica es y(t)=2e^-t+t-1
tex =[0:0.01:3];
yex = @(t)2*exp(-t)+t-1;
Yex=yex (tex);
hold on
plot (tex,Yex)
plot (T,Y,'.k')