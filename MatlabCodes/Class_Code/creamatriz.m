function A = creamatriz(n)
% crear una matriz nxn con
% 1 en la diagonal ppal 0
% 1/2 en las subdiagoalees +-1
% 1/3 en las subdiagonales +-2
%...
% 1/n en las subdiagonales +. n-1
% rellenamos la diag ppal
A = diag(ones(n,1));
% rellenamos las subdiagonales desde la  1 hasta
% la n-1
for i=1:n-1
    e=ones(n-i,1)/(i+1);
    A = A + diag(e,i)+diag(e,-i);
end

end