%function interpgamma
% Apartado 1
% dibujo funci�n gamma en [4,6]
xs=4:0.01:6;
ys=gamma(xs);
plot(xs,ys),
title('Funci�n Gamma'),

% estimaci�n num�rica derivadas en x=4 y x=6 mediante cociente incremental
disp('Estimaci�n incremental de Gamma(4)')
d4=(gamma(4+10^(-4))-gamma(4))/10^(-4);
disp(d4)

disp('Estimaci�n incremental de Gamma(6)')
d6=(gamma(6+10^(-4))-gamma(6))/10^(-4);
disp(d6)

%pause

% Apartado 2
% s = spline(x,y,xq) returns a vector of interpolated values s 
%   corresponding to the query points in xq.
%   The values of s are determined by cubic spline 
%   interpolation of x and y.
figure(2)
x=linspace(4,6,6);  % evaluamos la funci�n Gamma
y=gamma(x);         % en 6 puntos 
spl=spline(x,y,xs);
fun=gamma(xs);
plot(x,y,'ok',xs,fun,'b',xs,spl,'r--');shg
legend('nodos','y=\Gamma(x)','spline not-a-knot','Location','NW')
%shg
%pause 

% Apartado 3
% Spline SUJETO
% Spline Interpolation with Specified Endpoint Slopes 
%  Use clamped or complete spline interpolation when endpoint 
%  slopes are known. To do this, you can specify the values 
%  vector y with two extra elements, one at the beginning 
%  and one at the end, to define the endpoint slopes.
figure(3)
spl=spline(x,[d4 y d6],xs); % d4,d6: pendientes en los extremos 
                            % del intervalo
plot(x,y,'ok',xs,fun,'b',xs,spl,'r--');shg
legend('nodos','y=\Gamma(x)','spline sujeto','Location','NW')
%shg
%end

% ilustraci�n resultado con pendientes no adecuadas
figure(4)
spl=spline(x,[25 y 0],xs);
plot(x,y,'ok',xs,fun,'b',xs,spl,'r--');shg
legend('nodos','y=\Gamma(x)','spline sujeto','Location','NW')



% aproximaci�n num�rica de la derivada de x^2 en x=1
format short g
lista=[];
x2= @(x) x^2
for k=1:17
    CocInc=(x2(1+10^(-k))-x2(1))/10^(-k);
    ErrorAbs = CocInc-2;
    ErrorRel = norm(CocInc-2)/2;
    lista=[ lista ; k , CocInc, ErrorAbs, ErrorRel ];
end
disp('            n    Coc. Incr.  Error abs.  Error relat.')
disp(' ')
disp(lista)