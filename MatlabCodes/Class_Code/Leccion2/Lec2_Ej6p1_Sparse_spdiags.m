n=20000;

% usando matriz sparse (dispersa, no densa)
ejercicio6p1(n)
% sin usar matriz sparse
ejercicio6p1b(n)


% Uso de spdiags para matrices "dispersas" (sparse)
% A = spdiags(B,d,m,n) crea una matriz dispersa B, mxn,
%     cogiendo las columnas de B y colocándolas en las diagonales
%     especificadas en d.
function ejercicio6p1(n)
e=ones(n,1);
b=(1:n)';
A=spdiags([-e,2*e,-e],[-1,0,1],n,n);
tic
[L,U,P]=lu(A);
x=U\(L\(P*b));
toc
format long
disp('El valor de x(50) usando lu es')
disp(x(50))
end

% mismo problema sin usar matrices dispersas
function ejercicio6p1b(n)
e=ones(n,1);
b=(1:n)';
A=spdiags([-e,2*e,-e],[-1,0,1],n,n);
B=full(A);
tic
[L,U,P]=lu(B);
x=U\(L\(P*b));
toc
format long
disp('El valor de x(50) usando lu es')
disp(x(50))
end