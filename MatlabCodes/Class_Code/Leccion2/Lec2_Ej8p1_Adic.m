% Ejercicio adicional 8.1 de la Lecci�n 2
xfin = 550+30*17;  % �ltima abscisa
xi = 555:30:xfin;  % abscisas de los 17 puntos                      
yi = [0.644 0.649 0.646 0.655 0.633 0.676 0.678 0.699 ... % ordenadas 
      0.763 1.044 2.169 1.211 0.672 0.607 0.603 0.601 0.608];

x840=840;     % valor de x en el que evaluamos el spline
spl=spline(xi,yi,x840)
% dibujo de los puntos y del "aproximado" por el spline
figure(1)
plot(xi,yi,'ok',x840,spl,'*r'); %shg

xdib = linspace(555,xfin,500); % tomamos 500 abscisas del intervalo
spl2 = spline(xi,yi,xdib);     % para representar el spline
figure(2)
plot(xi,yi,'ok',x840,spl,'*r',xdib,spl2,'b-');



