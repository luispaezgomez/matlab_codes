% Ejercicio 7.1 de la lecci�n 2
% Gr�fica de un polinomio de interpolaci�n

close all;
clear variables;
format short g
% Tabla interpolaci�n: 
%  como hay cinco puntos, el polinomio ser� de grado 4
X=[-2 -1 0 2 5]';
Y=[0 3 5 11/2 6]';

xi=[-2:0.01:5]';  % mallado en x (del intervalo [-2,5])

% p = polyfit(x,y,n) returns the coefficients for a polynomial p(x) 
%     of degree n that is a best fit (in a least-squares sense) 
%     for the data in y. The coefficients in p are in descending 
%     powers, and the length of p is n+1

a=polyfit(X,Y,4) %  calcula el polinomio de interpolaci�n.
                 %  Los coeficientes los guarda en orden decreciente
                 %  en un vector fila.
                
% y = polyval(p,x) evaluates the polynomial p at each point in x. 
% The argument p is a vector of length n+1 whose elements are 
% the coefficients (in descending powers) of an nth-degree polynomial.
yi=polyval(a,xi); % eval�a el polinomio en los puntos del mallado
plot(X,Y,'o',xi,yi)
set(gca,'FontSize',18)



% determinaci�n del polinomio de interpolaci�n resolviendo el sistema
% lineal con la matriz de Vandermonde correspondiente
N=length(X);
V=[ X.^4 X.^3 X.^2 X ones(N,1)];
% resoluci�n de V a = Y
aVdm=(V\Y)  % coeficientes del polinomio en vector columna

dif=norm(a-aVdm')  % comprobaci�n de que coinciden con los de polyfit


