function [fval]=funL3E5p1_nojac(P)
% funcion del sistema no lineal
% output: fval y el jacobiano en P
x=P(1);
y=P(2);
fval = [ 2*x-y-exp(-x) ;  ...
        -x+2*y-exp(-y)];
end