% Ejercicio 3.1 
format long g
%% apartado 1
% mínimo de exp(-x)+x^2
% función anónima VECTORIAL
f = @(x) exp(-x) + x.^2;
% Plot de la función 
x=[-2:0.01:2];
plot(x,f(x),'b','LineWidth',2)
set(gca,'FontSize',18)

figure(2)
fplot(f,[-2 2])

df = @(x) -exp(-x) + 2*x;
% Partimos de x0
x0=0.5;
x1=fzero(df,x0)


%% apartado 2

% función anónima VECTORIAL
g = @(x) x.*exp(x) - 1/2;
figure(3)
fplot(g,[0 5])

x0=0.5;
x2=fzero(g,x0)

% diferencia entre las soluciones de ambos apartados
diff=norm(x1-x2)