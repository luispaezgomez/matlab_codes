% Ejercicio 3.1 
% soluciones de exp(x)*(x-1)=x
% función anónima VECTORIAL
f = @(x) exp(x).*(x-1)-x;
% Plot de la función junto con un eje
x=[-2:0.01:2];
plot(x,f(x),'b',x,zeros(size(x)),'r','LineWidth',2)
set(gca,'FontSize',18)
% vemos que tiene dos ceros. Uno cerca de -1 y el otro 
% cerca de 1.5

% Partimos de dos puntos iniciales distintos
% primero partimos de x0=1
x0=1;
x1=fzero(f,x0)
%
x0=20;
x2=fzero(f,x0)

pause

%
tol=1e-8;
opciones=optimset('TolX',tol,'Display','iter');
%
x0=1;
x1=fzero(f,x0,opciones)
%
x0=20;
x2=fzero(f,x0,opciones)

% Aunque el problema no lo pide, 
% calculamos el cero negativo
x0=-1;
x3=fzero(f,x0,opciones)

% dibujamos en la misma gráfica los dos ceros
hold on
plot(x3,0,'sk',x1,0,'sk', ...  % s=square, k=black
     'LineWidth',2,...               
     'MarkerSize',10)
hold off
