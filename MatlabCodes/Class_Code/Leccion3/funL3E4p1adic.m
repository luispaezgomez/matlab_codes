function [fval,jac]=funL3E4p1adic(P)
% función del sistema no lineal (definida como vector columna)
% output: fval y el jacobiano en P
x=P(1);
y=P(2);
fval = [x.^2+y^2-4 ;  ...
        x.*y-1];
jac = [ 2*x    2*y ;...
         y      x  ];
end