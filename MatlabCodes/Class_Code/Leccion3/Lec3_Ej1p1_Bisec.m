% Ejercicio 1.1 de la lección 3
% Búsqueda de ceros mediante bisección

close all;clear variables;

format long g

f = @(x) x-1-x.*exp(-x); % definición de la función

% mallado fino para el dibujo
x=[-2:0.01:4];
figure(1)       % dibujo de f(x) y de la recta y=0
plot(x,f(x), x,zeros(size(x)),'r','LineWidth',2)
set(gca,'FontSize',18)
% plot(x,f(x), ...
% %'LineWidth',2, ...
%      x,zeros(size(x)),'r')
hold on   % podremos seguir dibujando más cosas en esta misma gráfica
pause(5)

% empleamos el algoritmo de biseccion

% primer cero
a=-2;
b=0;
disp('signo')
prodextremos=f(a)*f(b)  % comprobamos que hay cambio de signo
tol=1e-16;  % tolerancia (máximo tamaño del intervalo final)
tic
[x1b,iter]=biseccion(f,a,b,tol)
time2=toc
LongitudIntervalo=x1b(2)-x1b(1)

% segundo cero

a=1;
b=2;
disp('signo')   
prodextremos=f(a)*f(b) % comprobamos que hay cambio de signo
tol=1e-8;    % tolerancia
[x2b,iter]=biseccion(f,a,b,tol)
LongitudIntervalo=x2b(2)-x2b(1)

%Plot
plot(x2b(2),0,'s',x1b(2),0,'s',...  % marca con cuadraditos los ceros                
    'LineWidth',2,...               % s=square
    'MarkerSize',10,...
    'MarkerEdgeColor','g',...       % g=green
    'MarkerFaceColor',[0.5,0.5,0.5]) % [0.5,0.5,0.5]=gris
pause(3)    % pausa de 3 segundos

% solucion con fzero fijando la tolerancia
% x = fzero(fun,x0) tries to find a point x where fun(x) = 0. 
%   This solution is where fun(x) changes sign.
%   (fzero cannot find a root of a function such as x^2).

tolfzero=1e-16;
opt=optimset('TolX',tolfzero);  
tic
x1=fzero(f,-1,opt);
time1=toc
x2=fzero(f,1,opt);

% plot de la funcion y los ceros con fsolve

plot(x1,0,'ro',x2,0,'ro','MarkerSize',15)  % marca con circulitos rojos los ceros
hold off

% comparamos las soluciones de biseccion y fsolve
disp('Bisec vs fzero')
diferencia=[x1b(1)-x1 x2b(1)-x2]

hold off

% solucion con fzero fijando la tolerancia
% [x,fval,exitflag,output] = (fun,x0,options)
% returns fun(x) in the fval output, 
% exitflag encoding the reason fzero stopped,and   
% an output structure containing information on the solution process.

tolfzero=1e-16;
opt=optimset('TolX',tolfzero,'Display','iter'); 
      % queremos ver más información sobre el proceso iterativo
tic
[x1,fval,flag,out]=fzero(f,-1,opt);
time1=toc
[x2,fval,flag,out]=fzero(f,1,opt);

% información extra solicitada
fval  % valor de la función en su cero aproximado
flag  % si flag vale 1, se ha conseguido la convergencia
out   % información sobre el proceso seguido para obtener la solución



%%
function [sol,iter] = biseccion(f,a,b,TOL)
% Metodo de bisección
% partiendo de un intervalo inicial [a b] donde hay un cambio de signo
% iteramos hasta que la longitud del intervalo sea menor que TOL
%
iter = 0;
while (b-a > TOL)
    c = (a + b)/2; % punto medio del intervalo
    if f(c)==0     % c es el cero
        a=c;b=c;
      elseif sign(f(c))==sign(f(b))  % compara signos entre c y b
        b=c;  % seleccionamos el subintervalo de la izquierda
      else
        a=c;  % seleccionamos el intervalo de la derecha
    end
    iter = iter + 1;
end
sol=[a,b]; % en output el intervalo refinado

end