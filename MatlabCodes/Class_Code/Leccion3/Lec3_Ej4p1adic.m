% Ejercicio 4.1 de la lección 3
% solución de un sistema no lineal:
%    x^2+y^2  = 4
%    xy = 1

[X,Y]=meshgrid([-2.1:0.1:2.1]);  % mallado en el plano 
Z1 = X.^2+Y.^2-4; % f1
Z2 = X.*Y-1; % f2
Z0 = zeros(size(X)); % z=0

figure(1)
% surf(X,Y,Z) creates a three-dimensional surface plot, 
%    which is a three-dimensional surface that has solid edge colors 
%    and solid face colors. The function plots the values in matrix Z 
%    as heights above a grid in the x-y plane defined by X and Y. 
%    The color of the surface varies according to the heights 
%    specified by Z.
surf(X,Y,Z1)
pause(1)
hold on
surf(X,Y,Z2)
pause(1)
surf(X,Y,Z0)
hold off
colorbar       % cambia el color según la altura
set(gca,'FontSize',14)


% Punto de intersección
% Solución del sistema no lineal
% opción SIN JACOBIANO ANALÍTICO

% x = fsolve(fun,x0) starts at x0 and tries to solve the equations 
%   fun(x) = 0, an array of zeros.
format long g
%P0=rand(2,1); % valor inicial aleatorio
P0=[2;2]; % valor inicial
[Psol,fval,flag,out] = ...
    fsolve(@funL3E4p1adic_nojac,P0)

% Punto de intersección
% Solución del sistema no lineal
format long g
% DerivativeCheck compare user-supplied derivatives 
%    and the values are 'on' or 'off'.
% Jacobian: si se proporciona el jacobiano hay que poner 'on'. La 
%    opción por defecto es 'off').
opciones = optimset('DerivativeCheck','on',...
           'Jacobian','on');
P0=[2;2]; % valor inicial (vector columna)
[Psol,fval,flag,out,jac] = ...
    fsolve(@funL3E4p1adic,P0,opciones)

% contour plot

figure(2)
contour(X,Y,Z1,[0,0],'b','Linewidth',2)  % curva nivel f1=0 en azul
axis([-2.1 2.1 -2.1 2.1])
axis equal
hold on
contour(X,Y,Z2,[0,0],'r','Linewidth',2)  % curva nivel f2=0 en rojo
plot(P0(1),P0(2),'og','Linewidth',2)   % punto inicial circulito verde
plot(Psol(1),Psol(2),'ok','Linewidth',2) % solución circulito negro
hold off
set(gca,'FontSize',14)