% Ejercicio 2.1 adicional de la lección 3
% Primeros pasos del método de Newton

% función anónima VECTORIAL
f = @(x) x.^2-2; 

% algoritmo de Newton
df= @(x) 2*x; % derivada analítica

format rat
x0=1;
x1b=newtonescalarRAT(f,df,x0,3)



%%
function x=newtonescalarRAT(fun,dfun,x0,numiter)
% método de Newton para ecuaciones escalares:
% fun y dfun son la función y su derivada,
% x0 es la estimación inicial para el cero.
% El criterio de parada es el número de iteraciones,
% no hace estimación del error
format rat
for k = 1:numiter
    d = fun(x0)/dfun(x0);
    x = x0-d
    x0 = x;
end
end