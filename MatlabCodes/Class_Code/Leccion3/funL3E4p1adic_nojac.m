function [fval]=funL3E4p1adic_nojac(P)
% funcion del sistema no lineal
% output: fval y el jacobiano en P
x=P(1);
y=P(2);
fval = [ x.^2+y^2-4 ;  ...
        x.*y-1];
end