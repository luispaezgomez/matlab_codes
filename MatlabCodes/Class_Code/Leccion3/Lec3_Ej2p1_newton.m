% Ejercicio 2.1 de la lección 3
% Búsqueda de ceros

% función anónima VECTORIAL
f = @(x) 1./(1+x.^2)-0.3; 
x=[-4:0.01:4]';
opt=optimset('TolX',1e-8);
[x1,fval,flag,out]=fzero(f,1,opt);
% ceros con fzero
plot(x,f(x),...
    x,zeros(size(x)),'r',...
    x1,0,'ro')


% algoritmo de newton
df= @(x) -2*x ./(1+x.^2).^2; % derivada analitica
% otra opcion es una derivada numerica
% diferencias centradas
% h=1e-4;
% df = @(x) (f(x+h)-f(x-h))/(2*h);
% otras opciones: derivada progresiva o regresiva
x0=0.2;
x1b=newtonescalar(f,df,x0,10)


%%
function x=newtonescalar(fun,dfun,x0,numiter)
% método de Newton para ecuaciones escalares:
% fun y dfun son la función y su derivada,
% x0 es la estimación inicial para el cero.
% El criterio de parada es el número de iteraciones,
% no hace estimación del error
for k = 1:numiter
    d = fun(x0)/dfun(x0);
    x = x0-d;
    fprintf('x(%2d)=%16.15f \n',k,x)  % Write data to text file
      % %d: Notación decimal (en base diez) para enteros (con signo).
      %   %2d  2 cifras
      %   aplicable al primer argumento, k
      % %f: Notación decimal con punto fijo.
      %   %16.15f  16 cifras, 15 decimales
      %   aplicable al segundo argumento, x
      % \n: salto de línea.
    pause(0.5) % pausa para ir viendo la iteración en pantalla
    x0 = x;
end
end


