% Ejercicio adicional 2.2 de la lección 3
% Búsqueda de ceros

% función anónima VECTORIAL
f = @(x) sign(x-2).*sqrt(abs(x-2)); 
x=[1:0.002:4];

plot(x,f(x),'b',x,zeros(size(x)),'r','LineWidth',2)
set(gca,'FontSize',18)
shg

x0=3;
x1=4;
niter=20;
xzero=secante(f,x0,x1,niter)



%% Función Secante
function xzero=secante(f,x0,x1,n)
% iteracion de secante
x=zeros(n+2,1); % inicializamos a 0
x(1)=x0; % valor inicial
x(2)=x1; % valor inicial
fprintf('x(%2d)=%16.15f \n',1,x(1))
fprintf('x(%2d)=%16.15f \n',2,x(2))
for k=1:n
    x(k+2)=x(k+1)-f(x(k+1))*(x(k+1)-x(k))/(f(x(k+1))-f(x(k)));
    fprintf('x(%2d)=%16.15f \n',k+2,x(k+2))  % Write data to text file
      % %d: Notación decimal (en base diez) para enteros (con signo).
      %   %2d  2 cifras
      %   aplicable al primer argumento, k+2
      % %f: Notación decimal con punto fijo.
      %   %16.15f  16 cifras, 15 decimales
      %   aplicable al segundo argumento, x(k+2)
      % \n: salto de línea.
end
% disp(' ')
% disp('Iteracion||Cero relativo||Cambio')
% [ [1:n+2]' x  [0;diff(x)]]
xzero=x(n+2); % ultima iteracion
end
