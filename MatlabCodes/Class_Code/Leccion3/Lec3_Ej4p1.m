% Ejercicio 4.1 de la lección 3
% solución de un sistema no lineal:
%    2x - y  = exp(-x)
%    -x + 2y = exp(-y)

% [X,Y] = meshgrid(x,y) returns 2-D grid coordinates based on 
%    the coordinates contained in vectors x and y. 
%    X is a matrix where each row is a copy of x, and 
%    Y is a matrix where each column is a copy of y. 
%    The grid represented by the coordinates X and Y has 
%    length(y) rows and length(x) columns.
% [X,Y] = meshgrid(x) is the same as [X,Y] = meshgrid(x,x), 
%    returning square grid coordinates with grid size 
%    length(x)-by-length(x).
[X,Y]=meshgrid([-1:0.1:1]);  % mallado en el plano 
Z1 = 2*X-Y-exp(-X); % f1
Z2 = 2*Y-X-exp(-Y); % f2
Z0 = zeros(size(X)); % z=0

figure(1)
% surf(X,Y,Z) creates a three-dimensional surface plot, 
%    which is a three-dimensional surface that has solid edge colors 
%    and solid face colors. The function plots the values in matrix Z 
%    as heights above a grid in the x-y plane defined by X and Y. 
%    The color of the surface varies according to the heights 
%    specified by Z.
surf(X,Y,Z1)
pause(1)
hold on
surf(X,Y,Z2)
pause(1)
surf(X,Y,Z0)
hold off
colorbar    % cambia el color según la altura
set(gca,'FontSize',14)

% Punto de intersección
% Solución del sistema no lineal
% opción SIN JACOBIANO ANALÍTICO

% x = fsolve(fun,x0) starts at x0 and tries to solve the equations 
%   fun(x) = 0, an array of zeros.
format long g
P0=rand(2,1); % valor inicial aleatorio
[Psol,fval,flag,out] = ...
    fsolve(@funL3E4p1_nojac,P0)

% Punto de intersección
% Solución del sistema no lineal
format long g
% DerivativeCheck compare user-supplied derivatives 
%    and the values are 'on' or 'off'.
% Jacobian: si se proporciona el jacobiano hay que poner 'on'. La 
%    opción por defecto es 'off').
opciones = optimset('DerivativeCheck','on',...
           'Jacobian','on');
P0=rand(2,1); % valor inicial aleatorio
[Psol,fval,flag,out,jac] = ...
    fsolve(@funL3E4p1,P0,opciones)

% contour plot

figure(2)
contour(X,Y,Z1,[0,0],'b','Linewidth',2)   % curva nivel f1=0 en azul
axis equal
hold on
contour(X,Y,Z2,[0,0],'r','Linewidth',2)   % curva nivel f2=0 en rojo
plot(P0(1),P0(2),'og','Linewidth',2)   % punto inicial circulito verde
plot(Psol(1),Psol(2),'ok','Linewidth',2) % solución circulito negro
hold off
set(gca,'FontSize',14)

