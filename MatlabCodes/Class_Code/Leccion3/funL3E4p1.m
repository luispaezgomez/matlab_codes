function [fval,jac]=funL3E4p1(P)
% función del sistema no lineal
% output: fval y el jacobiano en P
x=P(1);
y=P(2);
fval = [ 2*x-y-exp(-x);...
    -x+2*y-exp(-y)];
jac = [ 2+exp(-x)    -1      ;...
           -1     2+exp(-y)];
end