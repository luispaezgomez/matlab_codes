f = @(x) 1./ (1+x.^2);
xmin=-5;
xmax=5;
N=5;
h=(xmax-xmin)/N; % CON LINSPACE MEJOR
X=[xmin:h:xmax]';
Y=f(X);
[X Y]
xi=[xmin:0.01:xmax];

pn=polyfit(X,Y,N)
subplot(211)
plot(X,Y,'ro-',...
    xi,f(xi),'b',...
    xi,spline(X,Y,xi),'k')
subplot(212)
plot(xi,f(xi)-spline(X,Y,xi))