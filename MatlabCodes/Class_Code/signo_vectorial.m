function out = signo_vectorial(x)
% devuelve el signo de cada elemento del vector x
n=length(x); % cuenta el numero de elementos
out = zeros(n,1); % inicialiamos como columna de ceros
for i=1:n
    out(i) = funL1e10p1(x(i));
end
end