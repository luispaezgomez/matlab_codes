% Lección 4, Ejercicio 4.1 (ampliado)
% Aplicamos el método de Euler, Euler mejorado y 
%   el de Runge-Kutta de cuarto orden (RK4) al PVI   
%       y' - y = t*cos(t), y(0)=0

% Estudiamos el comportamiento del error con h

% EL METODO DE EULER ES CONVERGENTE Y SU ORDEN ES
%  a = 1 => err(h) ~ h^1    (convergencia lenta)
% El de Euler mejorado es convergente de orden 2: err(h) ~ h^2
% El de RK4 es convergente de orden 4: err(h) ~ h^4 

format short g

f = @(t,y) y+t.*cos(t); % miembro derecho de la EDO
y0 = 0;
h = 0.1;
t0 = 0;
tf = 1;
tspan = [t0 tf];
yex = @(t) 0.5*(t.*sin(t)+sin(t)-t.*cos(t)); % solución exacta del PVI

lista= [];
for k= 1:6
    [teuler,yeuler] = euler(f,t0,tf,y0,h);
    [teulermej,yeulermej] = eulermejorado(f,tspan,y0,h);
    [trk4,yrk4] = rk4(f,tspan,y0,h);
    
    lista=[lista; ...
        length(teuler)-1 ...  % número de pasos
        h ...                 % paso de integración
        max(abs(yex(teuler)-yeuler)) ...  % máximo error absoluto Euler
        max(abs(yex(teulermej)-yeulermej)) ...  % máximo error absoluto Euler mejorado
        max(abs(yex(trk4)-yrk4))];        % máximo error absoluto RK4
    h=h/10;
end
disp('        n pasos         h    Error Euler Er.Euler mej   Error RK4')
%disp(' ')
disp(lista)

% Dibujo en escala logarítmica del error frente a h
loglog( lista(:,2), lista(:,3),'ob-',...   % método Euler
        lista(:,2), lista(:,4),'or-',...   % método Euler mejorado
        lista(:,2), lista(:,5),'or-',...   % método RK4
        'Linewidth',2)
xlabel('h')
ylabel('Error')
set(gca,'FontSize',18)
legend('Euler','Euler mej.','RK4','Location','NorthWest')

fitEuler=polyfit(log(lista(:,2)), log(lista(:,3)),1)
fitEulermej=polyfit(log(lista(:,2)), log(lista(:,4)),1)
fitRK4=polyfit(log(lista(1:3,2)), log(lista(1:3,5)),1)

%%
function [T,Y]=euler(f,t0,tf,y0,h)
T=(t0:h:tf)';
N=length(T);
Y=zeros(N,length(y0));
y0=y0(:)';Y(1,:)=y0;
for j=1:N-1
   Y(j+1,:)=Y(j,:)+h*f(T(j),Y(j,:))';
end
end
%% Método Euler Mejorado
function [t,y]=eulermejorado(f,tspan,y0,h)
% metodo de euler mejorado en 1D
t=[tspan(1):h:tspan(2)]';
n=round((tspan(2)-tspan(1))/h);
y=zeros(n+1,1); % inicializamos a cero
y(1)=y0;
for k=1:n
    k1=f(t(k),y(k));
    k2=f(t(k)+h/2,y(k)+0.5*h*k1);
    y(k+1)=y(k)+h*k2;
end
end
%% Método de Runge-Kutta de cuarto orden (RK4) para una ecuación
function [t,y] = rk4(f,tspan,y0,h)
t=[tspan(1):h:tspan(2)]'; % malla de paso constante
n=round((tspan(2)-tspan(1))/h); % número subintervalos del mallado
y=zeros(n+1,1);  % inicializamos a cero el vector columna y
y(1)=y0;
for k=1:n
    k1=f(t(k),y(k));
    k2=f(t(k)+h/2,y(k)+0.5*h*k1);
    k3=f(t(k)+h/2,y(k)+0.5*h*k2);
    k4=f(t(k)+h,y(k)+h*k3);
    y(k+1)=y(k)+h*((1/6)*k1+(2/6)*k2+(2/6)*k3+(1/6)*k4);
end
end